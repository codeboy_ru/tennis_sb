from tornado.options import define, parse_command_line
import tornado.gen
import tornado.web
import tornado.ioloop
import tornado.httpserver

from windseed.settings import env, urls
from windseed.apps.tennis import handlers as web_handlers

import ui_methods

class Windseed(tornado.web.Application):
    """
    Our main application
    """
    def __init__(self):
        parse_command_line()

        # settings_ = dict(
        #     template_path=env.TEMPLATE_PATH,
        #     static_path=env.STATIC_PATH,
        #     cookie_secret=env.COOKIE_SECRET,
        #     xsrf_cookies=env.XSRF_COOKIES,
        #     debug=env.DEBUG,
        #     autoreload=env.AUTORELOAD,
        #     default_handler_class=web_handlers.ErrorHandler,
        #     default_handler_args=dict(status_code=404), )

        config = {
            'template_path': env.TEMPLATE_PATH,
            'static_path':env.STATIC_PATH,
            'st':env.STATIC_PATH,
            # 'cookie_secret':env.COOKIE_SECRET,
            # 'xsrf_cookies': env.XSRF_COOKIES,
            'xsrf_cookies': False,
            'debug': env.DEBUG,
            'autoreload': env.AUTORELOAD,
            'default_handler_class': web_handlers.ErrorHandler,
            'default_handler_args': dict(status_code=404),
            'cookie_secret' : 'sasasas',
            'ui_methods': ui_methods,
        }

        tornado.web.Application.__init__(self, urls.routes, **config)


def main():
    tornado.options.parse_command_line()
    ioloop = tornado.ioloop.IOLoop.instance()

    http_server = tornado.httpserver.HTTPServer(Windseed())
    # http_server.listen(8000, 'localhost')
    http_server.listen(8000)

    ioloop.start()


if __name__ == '__main__':
    main()
