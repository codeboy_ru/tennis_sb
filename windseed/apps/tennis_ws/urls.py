from windseed.apps.tennis_ws import (
    WebSocketHandler,
    WS_TestDataHandler
)

routes = [
    (r'/ws/?', WebSocketHandler.WSHandler),
    (r'/wst/', WS_TestDataHandler.WSHandler),

]