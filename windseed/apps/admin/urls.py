from tornado.web import url

from windseed.apps.admin import (
    AdminHandler,
    AdminEvents,
    AdminPlayersHandler,
    CourtHandler,
    XmlHandler,
    TourAdd,
    TablseHandler
)


routes = [
    url(r'/settings/?', AdminHandler.AdminHandler, name='settings'),
    (r'/settings/events/?(?P<ev_type>[^\/]+)?/?', AdminEvents.AdminEvents),
    (r'/settings/events/playerslist/(?P<tour_id>[^\/]+)/?', AdminEvents.PlayersList),
    (r'/settings/players/(?P<tour_id>[^\/]+)/?', AdminPlayersHandler.Players),
    (r'/settings/players/add/(?P<tour_id>[^\/]+)/', TourAdd.TourAdd),
    (r'/settings/profile/(?P<player_id>[^\/]+)/?', AdminPlayersHandler.PlayerProfile),
    (r'/settings/profilebylive/(?P<player_circ_id>[^\/]+)/?', AdminPlayersHandler.PlayerProfileByLiveID),
    (r'/settings/players/?', AdminPlayersHandler.Tournaments),
    (r'/courts/?', CourtHandler.CourtHandler),
    (r'/courts/(?P<court>[^\/]+)/?', CourtHandler.CourtHandler),

    (r'/settings/xml/?', XmlHandler.XmlCourtsList),
    (r'/settings/xml/error/(?P<ecourt_id>[^\/]+)/(?P<ecourt_alias>[^\/]+)/?', XmlHandler.XmlCourtsList),
    (r'/settings/xml/resetalias/(?P<court_uid>[^\/]+)/(?P<court_alias>[^\/]+)/?', XmlHandler.XmlResetCourtAlias),
    (r'/settings/xml/(?P<court_id>[^\/]+)/?', XmlHandler.XmlCourt),
    (r'/xml/(?P<court_alias>[^\/]+)/?', XmlHandler.XmlCourt),

    (r'/settings/createtables/?', TablseHandler.TablesHandler),
]
