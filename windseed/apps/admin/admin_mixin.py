from easysettings import EasySettings

from settings import t_settings


class GetSettings(object):
    settings_all = False
    settings_obj = False

    def __init__(self, *args, **kwargs):
        self.settings_obj = EasySettings(t_settings.ROOT_PATH+"tennis.conf")
        self.settings_all = self.get_setttings()
        self.context['set'] = self.settings_all
        super(GetSettings, self).__init__(*args, **kwargs)

    def get_setttings(self):
        all_s = self.settings_obj.list_settings()
        e_settings = dict()
        for i in all_s:
            e_settings[i[0]] = i[1]

        return e_settings

    def initialize(self, *args, **kwargs):
        # return super(GetSettings, self).initialize(*args, **kwargs)
        super(GetSettings, self).initialize(*args, **kwargs)
        self.context['set'] = self.settings_all

