import tornado.web
from time import gmtime, strftime
from datetime import timedelta, datetime
from windseed.apps.tennis.BaseHandler import BaseHandler
from tornado import gen
from easysettings import EasySettings

from settings import t_settings

import peewee
import json
from bs4 import BeautifulSoup
import requests
import argparse

import tornado.gen
import tornado.ioloop

from windseed.apps.tennis.models import Drawsheet, Player, Tournament
from selenium import webdriver
from pyvirtualdisplay import Display


from pymemcache.client.base import Client
client = Client(('localhost', 11211))


class TourAdd(BaseHandler):
    """
    Добавляем турнир и игроков из него в базу
    """
    tour_key = False
    cookies = False
    ds_base_url = 'http://www.itftennis.com/itf/web/usercontrols/tournaments/tournamentdrawsheets.aspx?eventid={0}&webletName=procircuit'
    tour = False

    @tornado.web.addslash
    def get(self, tour_id):
        # context = dict()
        context = self.context
        settings = self.settings_obj
        # settings.set("screen_status", False)

        tour_id = settings.get('tour_id')
        context['court_id'] = settings.get('court_id')
        context['match_id'] = settings.get('match_id')
        context['tour_id'] = tour_id
        context['screen_text'] = settings.get('screen_text')
        context['screen_status'] = settings.get('screen_status')
        context['color_status'] = settings.get('color_status')
        # print(context)

        self.tour_key = tour_id
        tour = self.get_tour()
        self.tour = tour

        self._parse_tour()
        # self.render('admin/admin.html', **context)
        self.redirect('/settings/players/')

    def get_tour(self):
        """
            создаём или просто берём турнир
        """
        try:
            tour = Tournament.get(Tournament.itf_code == self.tour_key)
        except Tournament.DoesNotExist:
            tour = Tournament.create(name = 'autoname - {0}'.format(self.tour_key), itf_code = self.tour_key)
            tour.save()
        # print(tour)
        return tour


    def _parse_tour(self):
        display = Display(visible=0, size=(800, 600))
        display.start()

        tour_key = self.tour_key
        url_tour_info = "http://www.itftennis.com/procircuit/tournaments/women's-tournament/info.aspx?tourkey={0}"\
            .format(tour_key)

        driver = webdriver.Firefox()
        driver.get(url_tour_info)
        driver.get_cookies()
        driver.refresh()
        html_source = driver.page_source
        # print(html_source)
        cookies = driver.get_cookies()
        # print(cookies)
        prep_cookies = dict()
        for cook in cookies:
            prep_cookies[cook['name']] = cook['value']

        soup = BeautifulSoup(html_source, 'html.parser')
        titleTag = soup.html.head.title
        if titleTag:
            # print(titleTag)
            # print(soup.prettify())
            ds_urls = soup.find_all('a', {'class':'lnkLinkToDrawsheet'})
            # print(ds_urls)
            for i in ds_urls:
                s = i['onclick']
                s = s[s.find("(")+1:s.find(")")]
                s = s.split(',')[0]
                # self.parse_ds(s, cookies)
                self.parse_ds(s, prep_cookies)
                # self.parse_ds(s)
        else:
            print('NOOOOOOOO')

        driver.close()
        display.stop()
        return True

    def parse_ds(self, ds_urls, cookies):
    # def parse_ds(self, ds_urls):
        """
            обрабатываем турнирную таблицу с игроками
            добавляем их в базу если нет и в турнир
            :param ds_urls:
            :param cookies:
        """
        # print('--------------------------------------------------')
        url= self.ds_base_url.format(ds_urls)
        s = requests.Session()
        page = s.get(url, cookies=cookies)
        # page = s.get(url)
        # print(page.url)

        soup = BeautifulSoup(page.content, 'html.parser')

        playesr_top = soup.find_all('a', {'id':'lnkPlayerTop1'})
        playesr_bot = soup.find_all('a', {'id':'lnkPlayerBottom1'})
        # print(playesr_top)
        # print(len(playesr_top))
        # print(len(playesr_bot))

        for i in playesr_top:
            name = i.string
            itf_id = i.get('href').split('=')[1]

            # добавляем игрока в базу
            try:
                player = Player.get(Player.itf_id == itf_id)
            except Player.DoesNotExist:
                player = Player.create(name=name, itf_name=name, itf_id=itf_id)
                player.save()

            # добавляем игрока в турнир
            try:
                ds = Drawsheet.get(Drawsheet.players == player.uid, Drawsheet.tournament == self.tour.uid)
            except Drawsheet.DoesNotExist:
                ds = Drawsheet.create(players=player.uid, tournament=self.tour.uid)
                ds.save()

        ### shit code mode on
        for i in playesr_bot:
            name = i.string
            itf_id = i.get('href').split('=')[1]

            # добавляем игрока в базу
            try:
                player = Player.get(Player.itf_id == itf_id)
            except Player.DoesNotExist:
                player = Player.create(name=name, itf_name=name, itf_id=itf_id)
                player.save()

            # добавляем игрока в турнир
            try:
                ds = Drawsheet.get(Drawsheet.players == player.uid, Drawsheet.tournament == self.tour.uid)
            except Drawsheet.DoesNotExist:
                ds = Drawsheet.create(players=player.uid, tournament=self.tour.uid)
                ds.save()
