import tornado.web
import json
from time import gmtime, strftime
from datetime import timedelta, datetime
from windseed.apps.tennis.BaseHandler import BaseHandler
from tornado import gen
# import asyncio
import requests
from easysettings import EasySettings

from settings import t_settings
from windseed.apps.tennis.models import Player, Tournament, Drawsheet, Court

from windseed.settings import db



from pymemcache.client.base import Client
client = Client(('localhost', 11211))


class TablesHandler(BaseHandler):

    @tornado.web.addslash
    def get(self):
        context = self.context

        self.create_tables()

        return self.redirect('/settings/')



    def create_tables(self):
        tables = [
            Player, Tournament, Drawsheet,
            Court
        ]

        db.pool.connect()

        db.pool.drop_tables(tables, safe=True, cascade=True)
        db.pool.create_tables(tables, safe=True)

        if not db.pool.is_closed():
            db.pool.close()

        return

