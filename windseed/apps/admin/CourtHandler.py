# coding: utf-8

import json
from time import gmtime, strftime
from windseed.apps.tennis.BaseHandler import BaseHandler
import tornado
from tornado import gen
# import asyncio
import requests
from easysettings import EasySettings

from windseed.apps.tennis.BaseAsyncHandler import AsyncRequestHandler

# from database.db_queries import get_users

class Handler(BaseHandler):
    def write_error(self, status_code, **kwargs):
        self.render('web/error.html', status_code=status_code)


class ErrorHandler(tornado.web.ErrorHandler, Handler):
    pass



class CourtHandler(BaseHandler):

    @tornado.web.addslash
    def get(self, court=False):
        context = dict()
        context = self.context
        # context['status'] = "work in progress"

        # context['courts'] = self._load_data()
        # context['tour'] = tour

        if court:
            # settings = EasySettings("tennis.conf")
            from settings import t_settings
            settings = EasySettings(t_settings.ROOT_PATH+"tennis.conf")
            settings.set("court_id", court)
            settings.save()

            # self.redirect('/settings')
            self.redirect(self.reverse_url('settings'))
        else:
            tour_get = self.get_argument("tour", default=None, strip=False)
            # print(tour_get)
            context['tour'] = tour_get
            if tour_get:
                # settings = EasySettings("tennis.conf")
                from settings import t_settings
                settings = EasySettings(t_settings.ROOT_PATH+"tennis.conf")

                itfname, context['courts'] = self._load_courts(tour_get)
                settings.set("tour_id", tour_get)
                settings.set("tour_name", itfname)
                settings.save()


            #-------------------
            self.render('admin/court.html', **context)



    def _load_courts(self, match_id):
        s = requests.Session()
        url = 'https://ls.sportradar.com/ls/feeds/?/itf/en/Europe:Berlin/gismo/tennis_tournament_info/itft-{0}'\
            .format(match_id)
        r = s.get(url)
        print('r1', r.url)

        tournament_info =r.json()
        tid = tournament_info['doc'][0]['data']['_tid']
        itfname = tournament_info['doc'][0]['data']['itfname']

        date_str = strftime("%Y%m%d", gmtime())

        url = 'https://ls.sportradar.com/ls/feeds/?/itf/en/Europe:Berlin/gismo/client_dayinfo/{0}'.format(date_str)
        # url = 'https://ls.sportradar.com/ls/feeds/?/itf/en/Europe:Berlin/gismo/client_dayinfo/{0}'.format('20160422')
        r2 = s.get(url)
        day_info = r2.json()
        # print(r2.url)
        day_info = day_info['doc'][0]['data']['matches']

        r_data = list()
        r_courts = list()

        for k,v in day_info.items():
            # if v.get('match') and v['match'].get('_tid') and v['match']['_tid'] == tid:
            if v.get('match') and v['match'].get('_tid') and v['param4'] == match_id:
                if v['match'].get('court'):
                    s_id = v['match']['status']['_id']
                    s_name = v['match']['status']['name']
                    court_name = v['match']['court']['name']
                    court_id = v['match']['court']['_id']
                    # print(s_id, s_name)
                    # print(court_id, court_name)
                    # print('~~~~~~~~~~~~~~~~~')

                    if not court_id in r_courts:
                        r_data.append({
                            'court_id':court_id,
                            'court_name':court_name
                        })
                        r_courts.append(court_id)

        return itfname, r_data


    def _load_data(self, match_id):
        s = requests.Session()
        # s.headers.update({
        #         'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:45.0) Gecko/20100101 Firefox/45.0'
        #     })

        # match_id = 'M-FU-FRA-09A-2016'
        url = 'https://ls.sportradar.com/ls/feeds/?/itf/en/Europe:Berlin/gismo/tennis_tournament_info/itft-{0}'\
            .format(match_id)
        r = s.get(url)

        tournament_info =r.json()
        tid = tournament_info['doc'][0]['data']['_tid']

        date_str = strftime("%Y%m%d", gmtime())

        url = 'https://ls.sportradar.com/ls/feeds/?/itf/en/Europe:Berlin/gismo/client_dayinfo/{0}'.format(date_str)
        # url = 'https://ls.sportradar.com/ls/feeds/?/itf/en/Europe:Berlin/gismo/client_dayinfo/{0}'.format('20160422')
        r2 = s.get(url)
        day_info = r2.json()
        # print(r2.url)
        # day_info = json.loads(r2.text)
        day_info = day_info['doc'][0]['data']['matches']


        r_data = list()

        for k,v in day_info.items():
            # print(v)
            # print('-------------------')
            if v.get('match') and v['match'].get('_tid') and v['match']['_tid'] == tid:
                r_game = dict()
                s_id = v['match']['status']['_id']
                # print('000000000000000000')
                # print(s_id)
                # print('============')

                if 1 < s_id < 80:
                    s_name = v['match']['status']['name']

                    p_home_name = v['match']['teams']['home']['name']
                    p_home_c_code = v['match']['teams']['home']['cc']['_id']
                    p_home_c_name = v['match']['teams']['home']['cc']['name']
                    p_home_c_sname = v['match']['teams']['home']['cc']['ioc']

                    p_away_name = v['match']['teams']['away']['name']
                    p_away_c_code = v['match']['teams']['away']['cc']['_id']
                    p_away_c_name = v['match']['teams']['away']['cc']['name']
                    p_away_c_sname = v['match']['teams']['away']['cc']['ioc']

                    court_name = v['match']['court']['name']
                    court_id = v['match']['court']['_id']

                    result = v['match']['result']

                    # print(k, court_name)
                    # print(s_name, s_id)
                    # print(p_home_name, p_away_name)
                    # print(result)
                    # print('---------------')

                    r_game = {
                        's_id':s_id,
                        's_name':s_name,
                        'court_name':court_name,
                        'court_id':court_id,
                        'p_home_name': p_home_name,
                        'p_home_c_sname':p_home_c_sname,
                        'p_away_name':p_away_name,
                        'p_away_c_sname':p_away_c_sname
                    }
                    r_data.append(r_game)

        return r_data





