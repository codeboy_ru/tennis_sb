
import json
from time import gmtime, strftime
from datetime import timedelta, datetime
from windseed.apps.tennis.BaseHandler import BaseHandler
from tornado import gen
# import asyncio
import requests
from easysettings import EasySettings

from settings import t_settings
from windseed.apps.tennis.models import Player, Drawsheet, Tournament


from pymemcache.client.base import Client
client = Client(('localhost', 11211))


class AdminEvents(BaseHandler):

    def get(self, **kwargs):
        context = dict()
        context = self.context

        settings = EasySettings(t_settings.ROOT_PATH+"tennis.conf")
        tour_id = settings.get('tour_id')
        context['court_id'] = settings.get('court_id')
        context['match_id'] = settings.get('match_id')
        context['tour_id'] = settings.get('tour_id')
        context['screen_text'] = settings.get('screen_text')
        context['screen_status'] = settings.get('screen_status')
        context['color_status'] = settings.get('color_status')

        # print('**** ', kwargs)
        event_type = kwargs.get('ev_type', False)
        self.event_type = event_type
        context['event_type'] = event_type

        courts = self.get_tour_info(tour_id)
        context['courts'] = courts

        #-------------------
        self.render('admin/admin_events.html', **context)


    def get_tour_info(self, tour_id):
        date_str = strftime("%Y%m%d", gmtime())

        url = 'https://ls.sportradar.com/ls/feeds/?/itf/en/Europe:Berlin/gismo/client_dayinfo/{0}'.format(date_str)
        s = requests.Session()
        r2 = s.get(url)
        day_info = r2.json()
        # print(r2.url)

        matches = day_info['doc'][0]['data']['matches']

        events = dict()
        courts_list = list()

        events, courts_list = self._parse_day(matches, events, courts_list, tour_id)
        # print(events)


        ######   DAY 2   ############
        # shit code mode on!
        delta = datetime.now() + timedelta(days=1)
        date_str2 = format(delta, '%Y%m%d')

        url = 'https://ls.sportradar.com/ls/feeds/?/itf/en/Europe:Berlin/gismo/client_dayinfo/{0}'.format(date_str2)
        s = requests.Session()
        r3 = s.get(url)
        # print(r3.url)
        day_info = r3.json()

        matches = day_info['doc'][0]['data']['matches']
        if len(matches) >=1:
            events, courts_list = self._parse_day(matches, events, courts_list, tour_id)

        ######   DAY -1   ############
        # delta = datetime.now() + timedelta(days=-1)
        # date_str2 = format(delta, '%Y%m%d')
        #
        # url = 'https://ls.sportradar.com/ls/feeds/?/itf/en/Europe:Berlin/gismo/client_dayinfo/{0}'.format(date_str2)
        # s = requests.Session()
        # r3 = s.get(url)
        # day_info = r3.json()
        #
        # matches = day_info['doc'][0]['data']['matches']
        # if len(matches) >=1:
        #     events, courts_list = self._parse_day(matches, events, courts_list, tour_id)


        # print(events)
        return events


    def _parse_day(self, matches, events, courts_list, tour_id):
        for k,v in matches.items():
            if v['param4'] == tour_id:
                if v['match'].get('court'):

                    # проверка на новый день
                    if self.event_type and self.event_type == 'new':
                        if v['match']['matchstatus'] == 'live' or v['match']['matchstatus'] == 'upcoming':
                            court_name = v['match']['court']['name']
                            court_id = v['match']['court']['_id']
                            team_home = v['match']['teams']['home']
                            team_away = v['match']['teams']['away']
                            status = {
                                'id': v['match']['status']['_id'],
                                'name': v['match']['status']['name'],
                                'matchstatus': v['match']['matchstatus'],
                            }

                            if court_id not in courts_list:
                                courts_list.append(court_id)
                                events[court_id] = {
                                    'court_id': court_id,
                                    'court_name': court_name,
                                    'events': list(),
                                }
                                aa = events[court_id]['events']
                                aa.append({
                                    'team_home': self._check_team(team_home, v, 'home'),
                                    'team_away': self._check_team(team_away, v, 'away'),
                                    'match': v,
                                    'status': status,
                                })

                            else:
                                aa = events[court_id]['events']
                                aa.append({
                                    'team_home': self._check_team(team_home, v, 'home'),
                                    'team_away': self._check_team(team_away, v, 'away'),
                                    'match': v,
                                    'status': status,
                                })

                    else:
                        court_name = v['match']['court']['name']
                        court_id = v['match']['court']['_id']
                        team_home = v['match']['teams']['home']
                        team_away = v['match']['teams']['away']
                        status = {
                            'id': v['match']['status']['_id'],
                            'name': v['match']['status']['name'],
                            'matchstatus': v['match']['matchstatus'],
                        }
                        if court_id not in courts_list:
                            courts_list.append(court_id)
                            events[court_id] = {
                                'court_id': court_id,
                                'court_name': court_name,
                                'events': list(),
                            }
                            aa = events[court_id]['events']
                            aa.append({
                                'team_home': self._check_team(team_home, v, 'home'),
                                'team_away': self._check_team(team_away, v, 'away'),
                                'match': v,
                                'status': status,
                            })

                        else:
                            aa = events[court_id]['events']
                            aa.append({
                                'team_home': self._check_team(team_home, v, 'home'),
                                'team_away': self._check_team(team_away, v, 'away'),
                                'match': v,
                                'status': status,
                            })

        return events, courts_list


    # todo: need some ref
    def _return_part_match(self, v, courts_list, events,):
        court_name = v['match']['court']['name']
        court_id = v['match']['court']['_id']
        team_home = v['match']['teams']['home']
        team_away = v['match']['teams']['away']
        status = {
            'id': v['match']['status']['_id'],
            'name': v['match']['status']['name'],
            'matchstatus': v['match']['matchstatus'],
        }
        if court_id not in courts_list:
            courts_list.append(court_id)
            events[court_id] = {
                'court_id': court_id,
                'court_name': court_name,
                'events': list(),
            }
            aa = events[court_id]['events']
            aa.append({
                'team_home': self._check_team(team_home, v, 'home'),
                'team_away': self._check_team(team_away, v, 'away'),
                'match': v,
                'status': status,
            })

        else:
            aa = events[court_id]['events']
            aa.append({
                'team_home': self._check_team(team_home, v, 'home'),
                'team_away': self._check_team(team_away, v, 'away'),
                'match': v,
                'status': status,
            })


    def _check_team(self, team, event, side='home'):

        if team.get('children'):
            player1 = {'name': team['children'][0]['name'],}
            player2 = {'name': team['children'][1]['name'],}
            if side == 'home':
                player1['live_id'] = event['param6']
                player2['live_id'] = event['param8']
            else:
                player1['live_id'] = event['param7']
                player2['live_id'] = event['param9']
            player1['in_base'] = self._check_player(player1['live_id'])
            player2['in_base'] = self._check_player(player2['live_id'])
            players = [player1, player2]
        else:
            player = {
                'name': team['name'],
            }
            if side == 'home':
                player['live_id'] = event['param6']
            else:
                player['live_id'] = event['param7']

            player['in_base'] = self._check_player(player['live_id'])
            players = [player]

        return players


    def _check_player(self, player_id):

        try:
            q_player = Player.get(Player.circ_id==player_id)
            return {
                'uid': q_player.uid,
                'name': q_player.name,
                'itf_id': q_player.itf_id,
                'live_id': q_player.circ_id,
            }
        except Player.DoesNotExist:
            return False



    def post(self, *args, **kwargs):
        pass


class PlayersList(BaseHandler):

    def get(self, tour_id=False):
        context = dict()
        # context = self.context
        context['tour_id'] = tour_id

        q_players = (Player
            .select()
            .join(Drawsheet)
            .join(Tournament)
            .where(Tournament.itf_code == tour_id)
            .order_by(Player.uid))

        r_list = list()
        for player in q_players:
            r_list.append({
                'uid': player.uid,
                'name': player.name,
                'itf_id': player.itf_id,
                'circ_id': player.circ_id,
            })
        context['list'] = r_list

        self.set_header('Content-Type', 'application/javascript')
        self.write(json.dumps(context
                                         # ,default=json_util.default)
                                         ))

    def post(self, *args, **kwargs):
        live_id = self.get_argument('liveid', False)
        uid = self.get_argument('uid', False)
        self.set_header('Content-Type', 'application/javascript')

        if live_id and uid:
            try:
                q_player = Player.get(uid=uid)
                q_player.circ_id = live_id
                q_player.save()
                self.write({'result':'good'})
            except Player.DoesNotExist:
                self.write({'result':'no'})

        else:
            self.write({'result':'no'})
