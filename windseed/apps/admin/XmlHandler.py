﻿from datetime import timedelta, datetime
from time import gmtime, strftime
from windseed.apps.tennis.BaseHandler import BaseHandler
from tornado import gen
import requests

import json
from easysettings import EasySettings

import tornado.web
import tornado.gen
import tornado.escape

from settings import t_settings
from windseed.apps.tennis.models import Court, Tournament


class Handler(BaseHandler):
    def write_error(self, status_code, **kwargs):
        self.render('web/error.html', status_code=status_code)


class ErrorHandler(tornado.web.ErrorHandler, Handler):
    pass


class XmlCourtsList(BaseHandler):
    """ список кортов """

    def get(self, ecourt_id=False, ecourt_alias=False, *args, **kwargs):
        context = self.context
        # court_alias = self.get_argument('alias', default=False, strip=False)


        context['ecourt_alias'] = ecourt_alias if ecourt_alias else False
        context['ecourt_id'] = ecourt_id if ecourt_id else False

        settings = self.settings_obj

        tour_id = settings.get('tour_id')
        context['court_id'] = settings.get('court_id')
        context['match_id'] = settings.get('match_id')
        tour_id = settings.get('tour_id')
        context['tour_id'] = tour_id

        courts = self._load_courts(tour_id)

        try:
            q_tour = Tournament.get(Tournament.itf_code == tour_id)
        except Tournament.DoesNotExist:
            q_tour = False

        q_courts = Court.select().where(Court.tour_name == tour_id)
        if len(q_courts) >= 1:
            for i in q_courts:
                for z in courts:
                    if z['court_id'] == i.live_id:
                        z['uid'] = i.uid
                        z['alias'] = i.alias
                        # z['tour_link'] = i.tour_link

        for court in courts:
            if not court.get('uid'):
                q_court = Court(
                    tour_name=tour_id,
                    live_name=court['court_name'],
                    live_id=court['court_id'],
                )
                if q_tour:
                    q_court.tour_link = q_tour.uid
                q_court.save()

        # context['courts'] = self._load_courts(tour_id)
        context['courts'] = courts
        msg = self.message_get()
        if msg: context['msg'] = msg

        # print(context)
        self.render('admin/admin_xml.html', **context)

    def _load_courts(self, match_id):
        s = requests.Session()
        url = 'https://ls.sportradar.com/ls/feeds/?/itf/en/Europe:Berlin/gismo/tennis_tournament_info/itft-{0}'\
            .format(match_id)
        r = s.get(url)
        # print('r1', r.url)

        tournament_info =r.json()
        tid = tournament_info['doc'][0]['data']['_tid']

        date_str = strftime("%Y%m%d", gmtime())
        url = 'https://ls.sportradar.com/ls/feeds/?/itf/en/Europe:Berlin/gismo/client_dayinfo/{0}'.format(date_str)
        r2 = s.get(url)
        day_info = r2.json()
        # print(r2.url)
        day_info = day_info['doc'][0]['data']['matches']

        r_data = list()
        r_courts = list()

        r_data, r_courts = self._parse_day(day_info, r_courts, r_data, match_id)

        ######   DAY 2   ############
        # shit code mode on!
        delta = datetime.now() + timedelta(days=1)
        date_str2 = format(delta, '%Y%m%d')

        url = 'https://ls.sportradar.com/ls/feeds/?/itf/en/Europe:Berlin/gismo/client_dayinfo/{0}'.format(date_str2)
        s = requests.Session()
        r3 = s.get(url)
        day_info = r3.json()
        day_info = day_info['doc'][0]['data']['matches']
        if len(day_info) >=1:
            r_data, r_courts = self._parse_day(day_info, r_courts, r_data, match_id)

        ######   DAY -1   ############
        delta = datetime.now() + timedelta(days=-1)
        date_str3 = format(delta, '%Y%m%d')

        url = 'https://ls.sportradar.com/ls/feeds/?/itf/en/Europe:Berlin/gismo/client_dayinfo/{0}'.format(date_str3)
        s = requests.Session()
        r4 = s.get(url)
        day_info = r4.json()
        day_info = day_info['doc'][0]['data']['matches']
        if len(day_info) >=1:
            r_data, r_courts = self._parse_day(day_info, r_courts, r_data, match_id)

        return r_data

    def _parse_day(self, day_info, r_courts, r_data, match_id):
        for k,v in day_info.items():
            if v.get('match') and v['match'].get('_tid') and v['param4'] == match_id:
                if v['match'].get('court'):
                    s_id = v['match']['status']['_id']
                    s_name = v['match']['status']['name']
                    court_name = v['match']['court']['name']
                    court_id = v['match']['court']['_id']

                    if not court_id in r_courts:
                        r_data.append({
                            's_id':s_id,
                            'court_id':court_id,
                            'court_name':court_name
                        })
                        r_courts.append(court_id)

        return r_data, r_courts

    def post(self, *args, **kwargs):
        court_uid = self.get_argument('court_uid', default=False, strip=False)
        court_id = self.get_argument('court_id', default=False, strip=False)
        court_alias = self.get_argument('court_alias', default=False, strip=False)

        if court_alias:
            try:
                q_court_exist = Court.get(alias=court_alias)
                self.message_set('Такой алиас уже существует!')
                # return self.redirect('/settings/xml/')
                return self.redirect('/settings/xml/error/{0}/{1}'.format(court_id, court_alias))
                # return self.redirect('/settings/xml/error/{0}/{1}'.format(q_court_exist.live_id, court_alias))
            except Court.DoesNotExist:

                try:
                    q_court = Court.get(uid=court_uid)
                    q_court.alias = court_alias
                    q_court.save()
                except Court.DoesNotExist:
                    pass
        return self.redirect('/settings/xml/')


class XmlResetCourtAlias(BaseHandler):

    def get(self, court_uid=False, court_alias=False):
        # court_uid = self.get_argument('court_uid', default=False, strip=False)
        # court_alias = self.get_argument('court_alias', default=False, strip=False)
        # print(court_alias)
        # print(court_uid)

        try:
            q_court = Court.get(live_id=court_uid)
            q_court.alias = court_alias
            q_court.save()
        except Court.DoesNotExist:
            pass

        return self.redirect('/settings/xml/')


class XmlCourt(BaseHandler):
    """ xml для корта """

    def check_data(self, court_id):
        """ проверяем есть ли в кэше данные о корте, и возвращяем, ну или нет ;) """
        r_data = self.mclient.get('xmldata:%s'%court_id)
        if r_data:
            # необходимо обязательно декодировать данные
            return json.loads(r_data.decode('utf-8'))
        else:
            r_data = self._load_court_info(court_id)
            # при сохранении словаря в мемкеш надо дампить его в json
            self.mclient.set('xmldata:%s'%court_id, json.dumps(r_data), expire=6)
            return r_data

    def get(self, court_id=False, court_alias=False):
        context = self.context
        r_data = False

        # если получили алиас корта надо забрать court_id из базы или мемкеша
        if court_alias:
            if self.mclient.get('alias:%s'%court_alias):
                r_data = self.check_data(self.mclient.get('alias:%s'%court_alias))
            else:
                try:
                    # если нет корта в памяти, то забираем его из базы и незабываем сохранить
                    q_court = Court.get(alias=court_alias)
                    r_data = self.check_data(q_court.live_id)
                    self.mclient.set('alias:%s'%court_alias, q_court.live_id)
                except Court.DoesNotExist:
                    # self.write_error(400)
                    self.return_empty_xml(court_id)
        else:
            r_data = self.check_data(court_id)

        if r_data and len(r_data) >=1 :
            r_data = r_data[0]

            # т.к. в подачах должны быть либо пробелы, либо звёздочки то делаем проверку
            if r_data['gs_who'] == '1':
                home_serve = '*'
                away_serve = ' '
            elif r_data['gs_who'] == '2':
                home_serve = ' '
                away_serve = '*'
            else:
                home_serve = ' '
                away_serve = ' '

            gs_home = 'AD' if r_data['gs_home'] == 50 else r_data['gs_home']
            gs_away = 'AD' if r_data['gs_away'] == 50 else r_data['gs_away']

            h1 = r_data['team_home'].get('children', False)
            a1 = r_data['team_away'].get('children', False)
            home_name = ''
            away_name = ''

            if not h1:
                home_name = '{0} [{1}]'.format(r_data['team_home']['name'], r_data['team_home']['cc']['a3'])
            else:
                # home_name = '{0} [{1}]'.format(r_data['team_home']['children'][0]['name'], r_data['team_home']['children'][0]['cc']['a3'])
                # home_name += ' | {0} [{1}]'.format(r_data['team_home']['children'][1]['name'], r_data['team_home']['children'][1]['cc']['a3'])
                home_name = r_data['team_home']['name']

            if not a1:
                away_name = '{0} [{1}]'.format(r_data['team_away']['name'], r_data['team_away']['cc']['a3'])
            else:
                # away_name = '{0} [{1}]'.format(r_data['team_away']['children'][0]['name'], r_data['team_away']['children'][0]['cc']['a3'])
                # away_name += ' | {0} [{1}]'.format(r_data['team_away']['children'][1]['name'], r_data['team_away']['children'][1]['cc']['a3'])
                away_name = r_data['team_away']['name']


            date_str = strftime("%d/%m/%Y %H:%M:%S", gmtime())
            response = """<?xml version="1.0" encoding="WINDOWS-1251"?>
    <line ID="1" Date="{date}" >
    <time>{time_h:02}:{time_m:02}</time>
    <court>{court_id}</court>
    <set1>{gs_home}</set1>
    <player1_serve>{home_serve}</player1_serve>
    <set2>{gs_away}</set2>
    <player2_serve>{away_serve}</player2_serve>
    <game1>{p1_home}</game1>
    <game2>{p2_home}</game2>
    <game3>{p3_home}</game3>
    <game4>{p1_away}</game4>
    <game5>{p2_away}</game5>
    <game6>{p3_away}</game6>
    <players1>{team_home}</players1>
    <players2>{team_away}</players2>
</line>""".format(**{
            'date': date_str,
            'court_id':r_data['court_id'],
            # 'gs_home':r_data['gs_home'],
            'gs_home':gs_home,
            # 'gs_away':r_data['gs_away'],
            'gs_away':gs_away,
            'home_serve': home_serve,
            'away_serve': away_serve,
            'p1_home': r_data['periods_p1']['home'],
            'p2_home': r_data['periods_p2']['home'],
            'p3_home': r_data['periods_p3']['home'],
            'p1_away': r_data['periods_p1']['away'],
            'p2_away': r_data['periods_p2']['away'],
            'p3_away': r_data['periods_p3']['away'],
            'team_home': home_name,
            'team_away': away_name,
            'time_h': r_data['time_div']['h'],
            'time_m': r_data['time_div']['m']
        })

            self.set_header('Content-Type', 'text/xml')
            self.write(response)

        else:
            # self.write_error(403)
            self.return_empty_xml(court_id)


    def return_empty_xml(self, court_id=False):
        date_str = strftime("%d/%m/%Y %H:%M:%S", gmtime())
        if not court_id: court_id = ' '

        response = """<?xml version="1.0" encoding="WINDOWS-1251"?>
    <line ID="1" Date="{date}" >
    <time> </time>
    <court>{court_id}</court>
    <set1> </set1>
    <player1_serve> </player1_serve>
    <set2> </set2>
    <player2_serve> </player2_serve>
    <game1> </game1>
    <game2> </game2>
    <game3> </game3>
    <game4> </game4>
    <game5> </game5>
    <game6> </game6>
    <players1> </players1>
    <players2> </players2>
</line>""".format(**{
            'date': date_str,
            'court_id':court_id,
        })

        self.set_header('Content-Type', 'text/xml')
        self.write(response)


    def _load_court_info(self, court_id):
        s = requests.Session()
        # print(court_id)

        date_str = strftime("%Y%m%d", gmtime())
        url = 'https://ls.sportradar.com/ls/feeds/?/itf/en/Europe:Berlin/gismo/client_dayinfo/{0}'.format(date_str)
        r2 = s.get(url)
        # print('++++++++++', r2.url)
        day_info = r2.json()
        day_info = day_info['doc'][0]['data']['matches']

        r_data = list()

        for k,v in day_info.items():
            # if v['match'].get('court') and v['match']['court']['_id'] == int(court_id):
            if v.get('match') and v['match'].get('_tid') and v['match'].get('court')\
                    and v['match']['court']['_id'] == int(court_id) and v['match']['matchstatus'] == 'live':
                r_game = dict()
                s_id = v['match']['status']['_id']

                if 1 < s_id <= 100:
                    # print(v['match'])
                    s_name = v['match']['status']['name']

                    team_home = v['match']['teams']['home']
                    team_away = v['match']['teams']['away']

                    time_played = v['match']['timeinfo']['played']
                    m, s = divmod(int(time_played), 60)
                    h, m = divmod(m, 60)
                    time_div = {'h':h, 'm':m, 's':s}


                    court_name = v['match']['court']['name']
                    court_id = v['match']['court']['_id']

                    result = v['match']['result']
                    if 0 < s_id < 100:
                        gs_who = v['match']['gamescore']['service']
                        gs_home = v['match']['gamescore']['home']
                        gs_away = v['match']['gamescore']['away']
                    else:
                        gs_who = 0
                        gs_home = 0
                        gs_away = 0

                    default_result = {
                        'home': ' ',
                        'away': ' '
                    }

                    periods_p1 = v['match']['periods'].get('p1', default_result)
                    periods_p2 = v['match']['periods'].get('p2', default_result)
                    periods_p3 = v['match']['periods'].get('p3', default_result)
                    periods_p4 = v['match']['periods'].get('p4')
                    periods_p5 = v['match']['periods'].get('p5')
                    match_id = v['_id']

                    r_game = {
                        's_id':s_id,
                        's_name':s_name,
                        'court_name':court_name,
                        'court_id':court_id,
                        'team_home':team_home,
                        'team_away':team_away,
                        'gs_who':gs_who,
                        'gs_home':gs_home,
                        'gs_away':gs_away,
                        'periods_p1': periods_p1,
                        'periods_p2': periods_p2,
                        'periods_p3': periods_p3,
                        'periods_p4': periods_p4,
                        'periods_p5': periods_p5,
                        'match_id': match_id,
                        'time_div': time_div,
                        'time_played': time_played,
                    }
                    r_data.append(r_game)

        return r_data

