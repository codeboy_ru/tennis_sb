import json
from PIL import Image
import io

from windseed.apps.tennis.BaseHandler import BaseHandler
from settings import t_settings as env

from pymemcache.client.base import Client
client = Client(('localhost', 11211))

import peewee
from peewee import DoesNotExist

from windseed.apps.tennis.models import Player, Drawsheet, Tournament

class Players(BaseHandler):

    def get(self, tour_id=False):
        context = dict()
        context = self.context
        context['tour_id'] = tour_id

        q_players = (Player
            .select()
            .join(Drawsheet)
            .join(Tournament)
            .where(Tournament.uid == tour_id)
            .order_by(Player.uid))

        r_list = list()
        for player in q_players:
            r_list.append({
                'uid': player.uid,
                'name': player.name,
                'itf_id': player.itf_id,
                'circ_id': player.circ_id,
            })
        context['list'] = r_list

        self.render('admin/admin_players.html', **context)


    def post(self, *args, **kwargs):

        self.redirect('/settings')


class Tournaments(BaseHandler):

    def get(self):
        context = dict()
        context = self.context

        tournaments = Tournament.select()
        r_list = list()
        for i in tournaments:
            r_list.append({
                'uid': i.uid,
                'name': i.name,
                'itf_code': i.itf_code
            })
        context['list'] = r_list
        self.render('admin/admin_players.html', **context)


    def post(self, *args, **kwargs):
        self.redirect('/settings')


class PlayerProfile(BaseHandler):

    def get(self, player_id=False):
        context = dict()
        context = self.context
        context['player'] = False
        context['msg'] = False

        if player_id:
            q_tours = (Tournament
                .select()
                .join(Drawsheet)
                .join(Player)
                .where(Player.uid == player_id)
                .order_by(Player.uid))
            r_list = list()
            for tour in q_tours:
                r_list.append({
                    'uid': tour.uid,
                    'name': tour.name,
                    'itf_id': tour.itf_id,
                    'itf_code': tour.itf_code,
                })
            context['tours'] = r_list

            try:
                q_player = Player.get(Player.uid == player_id)
                context['player'] = q_player
                self.render('admin/admin_profile.html', **context)
            except Player.DoesNotExist:
                self.redirect('/settings/')
        else:
            self.redirect('/settings/')


    def post(self, *args, **kwargs):
        player_id = kwargs.get('player_id')
        player_name = self.get_argument('player_name', default=False, strip=False)
        circ_id = self.get_argument('circ_id', default=False, strip=False)

        context = dict()
        context = self.context
        context['player'] = False
        try:
            q_player = Player.get(Player.uid == player_id)
            if player_name:
                q_player.name = player_name
            if circ_id:
                q_player.circ_id = circ_id
            q_player.save()

            context['player'] = q_player
        except Player.DoesNotExist:
            context['msg'] = 'Ошибка.'
            self.render('admin/admin_profile.html', **context)

        # сохраняем картинку в зависимости от формата
        try:
            file = self.request.files['filearg'][0]
            if file['content_type'] == 'image/jpeg': # если формат jpg
                output_file = open('{0}/{1}.jpg'.format(env.UPLOAD_PATH, q_player.itf_id), 'wb')
                output_file.write(file['body'])
                self.redirect('/settings/profile/{0}'.format(player_id))

            else: # если любой другой формат типа PNG
                image = Image.open(io.BytesIO(file['body']))
                convert_file = image.convert('RGB')
                convert_file.save('{0}/{1}.jpg'.format(env.UPLOAD_PATH, q_player.itf_id))
                self.redirect('/settings/profile/{0}'.format(player_id))

        except KeyError:
            context['msg'] = 'Ошибка.'
            self.redirect('/settings/profile/{0}'.format(player_id))


class PlayerProfileByLiveID(BaseHandler):

    def get(self, player_circ_id=False):
        context = dict()
        context = self.context
        context['player'] = False
        context['msg'] = False

        if player_circ_id:
            try:
                q_player = Player.get(Player.circ_id == player_circ_id)
                context['player'] = q_player
                self.redirect('/settings/profile/{0}'.format(q_player.uid))
            except Player.DoesNotExist:
                self.redirect('/settings/')
        else:
            self.redirect('/settings/')
