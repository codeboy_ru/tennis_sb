import peewee

from windseed.base.model import Model


class Player(Model):
    """
        модель игрока
    """
    name = peewee.CharField(max_length=256, index=True)
    itf_name = peewee.CharField(max_length=256, index=True)
    itf_id = peewee.CharField(max_length=256, index=True, null=True, unique=True)
    circ_id = peewee.CharField(max_length=256, index=True, null=True, unique=True)

    def __unicode__(self):
        return '{0} - {1} | {2}'.format(self.uid, self.name, self.itf_id)


class Tournament(Model):
    """
        модель турнира
    """
    name = peewee.CharField(max_length=256, index=True)
    itf_code = peewee.CharField(max_length=256, null=True)
    itf_id = peewee.CharField(max_length=256, null=True)

    def __unicode__(self):
        return '{0} - {1} | {2}'.format(self.uid, self.name, self.itf_code)


class Drawsheet(Model):
    """
        связь турнира с игроком
    """
    players = peewee.ForeignKeyField(Player,
        # db_column='players',
        to_field='uid',
        related_name='players',
        # on_delete='CASCADE',
        # index=True,
        # unique=True
    )
    tournament = peewee.ForeignKeyField(Tournament,
        # db_column='tournament',
        to_field='uid',
        related_name='tournaments',
        # on_delete='CASCADE',
        # index=True,
        # unique=True
    )

class Court(Model):
    live_name = peewee.CharField(max_length=256, index=True, help_text='имя корта в itf')
    live_id = peewee.IntegerField(help_text='id корта в itf')
    alias = peewee.CharField(max_length=256, null=True)
    tour_link = peewee.ForeignKeyField(Tournament, to_field='uid', null=True)
    tour_name = peewee.CharField(max_length=256, null=True)
