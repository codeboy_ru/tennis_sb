from windseed.apps.tennis import (
    HomeHandler,
    GamesHandler,
)

routes = [
    (r'/'+'?', GamesHandler.GameInfoHandler),
    (r'/scoreboard/(?P<param1>[^\/]+)/?', GamesHandler.GameInfoHandler),
    (r'/scoreboard-change/(?P<court_id>[^\/]+)/?', GamesHandler.GameChangesAH),
]
