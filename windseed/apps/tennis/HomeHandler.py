# coding: utf-8

import json
from time import gmtime, strftime
from windseed.apps.tennis.BaseHandler import BaseHandler
from tornado import gen
# import asyncio
import requests
from easysettings import EasySettings

from windseed.apps.tennis.BaseAsyncHandler import AsyncRequestHandler

# from database.db_queries import get_users


class HomeAHandler(BaseHandler):


    def get(self):
        context = dict()
        context = self.context
        # context['status'] = "work in progress"

        context['courts'] = self._load_data()

        # settings = EasySettings("tennis.conf")
        # settings.set("username", "cjw")
        # settings.set("firstrun", False)
        # print(settings.get('firstrun'))
        # settings.save()


        #-------------------
        self.render('main.html', **context)



    def _load_data(self):
        s = requests.Session()
        # s.headers.update({
        #         'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:45.0) Gecko/20100101 Firefox/45.0'
        #     })

        match_id = 'M-FU-FRA-09A-2016'
        url = 'https://ls.sportradar.com/ls/feeds/?/itf/en/Europe:Berlin/gismo/tennis_tournament_info/itft-{0}'\
            .format(match_id)
        r = s.get(url)

        tournament_info =r.json()
        tid = tournament_info['doc'][0]['data']['_tid']

        date_str = strftime("%Y%m%d", gmtime())

        url = 'https://ls.sportradar.com/ls/feeds/?/itf/en/Europe:Berlin/gismo/client_dayinfo/{0}'.format(date_str)
        # url = 'https://ls.sportradar.com/ls/feeds/?/itf/en/Europe:Berlin/gismo/client_dayinfo/{0}'.format('20160422')
        r2 = s.get(url)
        day_info = r2.json()
        # print(r2.url)
        # day_info = json.loads(r2.text)
        day_info = day_info['doc'][0]['data']['matches']


        r_data = list()

        for k,v in day_info.items():
            # print(v)
            # print('-------------------')
            if v.get('match') and v['match'].get('_tid') and v['match']['_tid'] == tid:
                r_game = dict()
                s_id = v['match']['status']['_id']

                if 1 < s_id < 80:
                    s_name = v['match']['status']['name']

                    p_home_name = v['match']['teams']['home']['name']
                    p_home_c_code = v['match']['teams']['home']['cc']['_id']
                    p_home_c_name = v['match']['teams']['home']['cc']['name']
                    p_home_c_sname = v['match']['teams']['home']['cc']['ioc']

                    p_away_name = v['match']['teams']['away']['name']
                    p_away_c_code = v['match']['teams']['away']['cc']['_id']
                    p_away_c_name = v['match']['teams']['away']['cc']['name']
                    p_away_c_sname = v['match']['teams']['away']['cc']['ioc']

                    court_name = v['match']['court']['name']
                    court_id = v['match']['court']['_id']

                    result = v['match']['result']

                    # print(k, court_name)
                    # print(s_name, s_id)
                    # print(p_home_name, p_away_name)
                    # print(result)
                    # print('---------------')

                    r_game = {
                        's_id':s_id,
                        's_name':s_name,
                        'court_name':court_name,
                        'court_id':court_id,
                        'p_home_name': p_home_name,
                        'p_home_c_sname':p_home_c_sname,
                        'p_away_name':p_away_name,
                        'p_away_c_sname':p_away_c_sname
                    }
                    r_data.append(r_game)

        return r_data





