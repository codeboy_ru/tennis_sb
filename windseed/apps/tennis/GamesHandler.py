﻿
from time import gmtime, strftime
from windseed.apps.tennis.BaseHandler import BaseHandler
from tornado import gen
import requests

import json
from easysettings import EasySettings
from settings import t_settings

# from apps.BaseAsyncHandler import AsyncRequestHandler
# from database.models import Flight
# from database.db_queries import get_flights


import peewee

import tornado.web
import tornado.gen
import tornado.escape

from settings import t_settings
from windseed.base import handler
from windseed.apps.tennis.models import Player



class Handler(BaseHandler):
    def write_error(self, status_code, **kwargs):
        self.render('web/error.html', status_code=status_code)


class ErrorHandler(tornado.web.ErrorHandler, Handler):
    pass


class GameInfoHandler(Handler):

    @tornado.web.addslash
    # @tornado.gen.coroutine
    def get(self, param1=False):
        context = dict()
        context = self.context

        context['ws_url'] = t_settings.WS_URL
        context['ws_port'] = t_settings.WS_PORT


        settings = EasySettings(t_settings.ROOT_PATH+"tennis.conf")
        court_id = settings.get('court_id')
        context['color_status'] = settings.get('color_status')
        context['screen_status'] = 'off'
        context['screen_text'] = settings.get('screen_text', default='Information will be updated soon')

        screen_status_raw = client.get('screen_status')
        if screen_status_raw:
            screen_status = screen_status_raw.decode(encoding='UTF-8')

            if screen_status == 'on':
                context['screen_status'] = 'on'

        if court_id:
            context['court_id'] = court_id
            matchs = self._load_data(court_id)
            context['matchs'] = matchs
            # print(matchs)
            if len(matchs) < 1:
                context['match'] = False
            else:
                context['match'] = matchs[0]
        else:
            context['court_id'] = False
            context['match'] = False

        #-------------------
        self.render('scoreboard.html', **context)



    def _load_data(self, court_id):
        s = requests.Session()
        # s.headers.update({
        #         'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:45.0) Gecko/20100101 Firefox/45.0'
        #     })

        date_str = strftime("%Y%m%d", gmtime())
        url = 'https://ls.sportradar.com/ls/feeds/?/itf/en/Europe:Berlin/gismo/client_dayinfo/{0}'.format(date_str)
        r2 = s.get(url)
        # print('++++++++++', r2.url)
        day_info = r2.json()
        day_info = day_info['doc'][0]['data']['matches']

        r_data = list()

        for k,v in day_info.items():
            if v.get('match') and v['match'].get('_tid') and v['match'].get('court')\
                    and v['match']['court']['_id'] == int(court_id) and v['match']['matchstatus'] == 'live':
                r_game = dict()
                s_id = v['match']['status']['_id']

                if 1 < s_id <= 100:
                    time_start = v['match']['timeinfo']['started']
                    time_played = v['match']['timeinfo']['played']
                    time_running = v['match']['timeinfo']['running']
                    time_ended = v['match']['timeinfo']['ended']
                    m, s = divmod(int(time_played), 60)
                    h, m = divmod(m, 60)
                    time_div = {'h':h, 'm':m, 's':s}

                    s_name = v['match']['status']['name']
                    team_home = v['match']['teams']['home']
                    team_away = v['match']['teams']['away']
                    court_name = v['match']['court']['name']
                    court_id = v['match']['court']['_id']

                    result = v['match']['result']
                    if 0 < s_id < 100:
                        gs_who = v['match']['gamescore']['service']
                        gs_home = v['match']['gamescore']['home']
                        gs_away = v['match']['gamescore']['away']
                    else:
                        gs_who = 0
                        gs_home = 0
                        gs_away = 0

                    default_result = {
                        'home': 0,
                        'away': 0
                    }

                    periods_p1 = v['match']['periods'].get('p1', default_result)
                    periods_p2 = v['match']['periods'].get('p2', default_result)
                    periods_p3 = v['match']['periods'].get('p3')
                    periods_p4 = v['match']['periods'].get('p4')
                    periods_p5 = v['match']['periods'].get('p5')

                    match_id = v['_id']

                    r_game = {
                        's_id':s_id,
                        's_name':s_name,
                        'court_name':court_name,
                        'court_id':court_id,
                        'team_home':team_home,
                        'team_away':team_away,
                        'gs_who':gs_who,
                        'gs_home':gs_home,
                        'gs_away':gs_away,
                        'periods_p1':periods_p1,
                        'periods_p2':periods_p2,
                        'periods_p3':periods_p3,
                        'periods_p4':periods_p4,
                        'periods_p5':periods_p5,
                        'match_id':match_id,
                        'p1_id': self._get_player_itfid(v['param6']),
                        'p2_id': self._get_player_itfid(v['param7']),
                        'p3_id': self._get_player_itfid(v['param8']),
                        'p4_id': self._get_player_itfid(v['param9']),
                        'time_start': time_start,
                        'time_played': time_played,
                        'time_div': time_div,
                    }
                    r_data.append(r_game)

        return r_data

    def _get_player_itfid(self, liveid):
        try:
            q_player = Player.get(Player.circ_id == liveid)
            return q_player.itf_id
        except Player.DoesNotExist:
            return None



from pymemcache.client.base import Client
client = Client(('localhost', 11211))


class GameChangesAH(BaseHandler):
    """ отдаёт словарь с данными для отрисовки JS """

    def get(self, court_id):
        context = dict()
        context = self.context

        message = ''
        screen_status = 'off'
        board_reload = 'off'
        r_data = ''

        # matchs = self._load_live_data(court_id)
        # match_status = client.get('match_status').decode(encoding='UTF-8')
        match_status = 'live'

        if screen_status == 'on':
            settings = EasySettings(t_settings.ROOT_PATH+"tennis.conf")
            message = settings.get('screen_text')

        else:
            screen_status = 'off'
            r_data = self._load_events(court_id)
            if r_data == 0:
                screen_status = 'wait'

        if client.get('board_reload') and client.get('board_reload').decode(encoding='UTF-8') == 'on':
            board_reload = client.get('board_reload').decode(encoding='UTF-8')
            client.set('board_reload', 'off')


        # print(screen_status)
        response = {
            'status':match_status,
            'message':message,
            'data':r_data,
            'screen_status':screen_status,
            'board_reload':board_reload
        }


        # self._load_events_data()

        self.write(response)



    def _load_events_data(self):
        """ OOOLLLLDDDDD """

        s = requests.Session()
        url = 'https://ls.sportradar.com/ls/feeds/?/itf/en/Europe:Berlin/gismo/event_getitf'
        r = s.get(url)
        events = r.json()
        events = events['doc'][0]['data']
        for event in events:
            # print('matchstatus: ', event['match']['matchstatus'])
            if event['matchid'] == 9370125:
                print('event id:', event['_id'], 'match id: ', event['matchid'])
                print('type id:', event['_typeid'], '| type: ', event['type'],'| name: ', event['name'])
                print('home: ', event['match']['teams']['home']['name'], '| home: ', event['match']['teams']['away']['name'])
                if event.get('service'): print('service:', event['service'], 'team:', event['team'])
                if event.get('set_score'): print('+++set_score: ', event['set_score'])
                if event.get('game_score'): print('===game_score: ', event['game_score'])
                if event.get('game_points'):print('---game_points: ', event['game_points'])
                print('matchstatus: ', event['match']['matchstatus'])
                if event.get('p'):print('OOOOOOOOOO - p:', event['p'])
                print('---------------------')

            if not client.get('event_ids_list'):
                client.set('event_ids_list', 'a')

            if not client.get('e-%s' % event['_typeid']):
                client.set('e-%s' % event['_typeid'], '%s,%s' % (event['type'], event['name']))
                client.append('event_ids_list', ',e-%s' % event['_typeid'])


    def _load_events(self, court_id):
        s = requests.Session()
        url = 'https://ls.sportradar.com/ls/feeds/?/itf/en/Europe:Berlin/gismo/event_getitf'
        r = s.get(url)

        events = r.json()
        events = events['doc'][0]['data']

        events_list = list()
        events_dict = dict()

        for event in events:
            # print('matchstatus: ', event['match']['matchstatus'])
            if event['match'].get('court') and event['match']['court']['_id'] == int(court_id):
                # print(event)
                ev_data = dict()
                ev_data['ev_type_type'] = event['type']
                ev_data['ev_type_id'] = event['_typeid']
                ev_data['ev_type_name'] = event['name']

                ev_data['match_id'] = event['matchid']

                # live, recentlyended, upcoming
                ev_data['match_status'] = event['match']['matchstatus']
                ev_data['court_name'] = event['match']['court']['name']
                ev_data['court_id'] = event['match']['court']['_id']

                ev_data['service'] = event.get('service', 0)

                ev_data['set_score'] = event.get('set_score', 0)
                ev_data['game_score'] = event.get('game_score', 0)
                ev_data['game_points'] = event.get('game_points', 0)

                ev_data['period'] = event.get('p', 0)

                ev_data['periods'] = event['match'].get('periods', 0)
                # print('per - ', event['match'].get('periods', 0))

                ev_data['full_event'] = event


                events_list.append(event['_id'])
                events_dict[event['_id']] = ev_data
                # print('###########')
                # print(events_list)
                # print(events_dict)
                # print('###########')

        if len(events_list) >= 1:
            max_event = max(events_list)
            response_data = events_dict[max_event]
            return response_data

        else:
            return 0




    def _load_live_data(self, court_id):
        s = requests.Session()
        # s.headers.update({
        #         'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:45.0) Gecko/20100101 Firefox/45.0'
        #     })

        date_str = strftime("%Y%m%d", gmtime())

        url = 'https://ls.sportradar.com/ls/feeds/?/itf/en/Europe:Berlin/gismo/client_dayinfo/{0}'.format(date_str)


        r2 = s.get(url)
        # print(r2.url)
        day_info = r2.json()
        day_info = day_info['doc'][0]['data']['matches']

        r_data = list()

        for k,v in day_info.items():
            # print('----', v['match']['court']['_id'] == int(court_id))
            if v.get('match') and v['match'].get('_tid') and v['match'].get('court')\
                    and v['match']['court']['_id'] == int(court_id) and v['match']['matchstatus'] == 'live':
                r_game = dict()
                s_id = v['match']['status']['_id']
                # print('=== ',s_id ,v['match']['status']['name'], v['match']['court']['_id'], int(court_id))
                # print('--- ', v['match']['matchstatus'])
                # client.set('match_status', 'live')

                if 0 < s_id <= 100:
                    s_name = v['match']['status']['name']

                    p_home_name = v['match']['teams']['home']['name']
                    p_home_c_code = v['match']['teams']['home']['cc']['_id']
                    p_home_c_name = v['match']['teams']['home']['cc']['name']
                    p_home_c_sname = v['match']['teams']['home']['cc']['ioc']
                    p_home_c_fname = v['match']['teams']['home']['cc']['a2']

                    p_away_name = v['match']['teams']['away']['name']
                    p_away_c_code = v['match']['teams']['away']['cc']['_id']
                    p_away_c_name = v['match']['teams']['away']['cc']['name']
                    p_away_c_sname = v['match']['teams']['away']['cc']['ioc']
                    p_away_c_fname = v['match']['teams']['away']['cc']['a2']

                    court_name = v['match']['court']['name']
                    court_id = v['match']['court']['_id']

                    result = v['match']['result']
                    if 0 < s_id < 100:
                        gs_who = v['match']['gamescore']['service']
                        gs_home = v['match']['gamescore']['home']
                        gs_away = v['match']['gamescore']['away']
                    else:
                        gs_who = 0
                        gs_home = 0
                        gs_away = 0

                    default_result = {
                        'home': 0,
                        'away': 0
                    }

                    periods_p1 = v['match']['periods'].get('p1', default_result)
                    periods_p2 = v['match']['periods'].get('p2', default_result)
                    periods_p3 = v['match']['periods'].get('p3')
                    periods_p4 = v['match']['periods'].get('p4')
                    periods_p5 = v['match']['periods'].get('p5')

                    if s_id == 100:
                        print('ooooooooo')
                        client.set('match_status', 'end')
                        print(periods_p1)
                        print(periods_p2)
                        print(periods_p3)
                        print(periods_p4)
                        print(periods_p5)
                    else:
                        client.set('match_status', 'live')

                    match_id = v['_id']

                    r_game = {
                        's_id':s_id,
                        's_name':s_name,
                        'court_name':court_name,
                        'p_home_name': p_home_name,
                        'p_home_c_sname':p_home_c_sname,
                        'p_home_c_fname':p_home_c_fname,
                        'p_away_name':p_away_name,
                        'p_away_c_sname':p_away_c_sname,
                        'p_away_c_fname':p_away_c_fname,
                        'gs_who':gs_who,
                        'gs_home':gs_home,
                        'gs_away':gs_away,
                        'periods_p1':periods_p1,
                        'periods_p2':periods_p2,
                        'periods_p3':periods_p3,
                        'periods_p4':periods_p4,
                        'periods_p5':periods_p5,
                        'match_id':match_id
                    }
                    r_data.append(r_game)

            # elif:
            #     client.set('match_status', 'end')

        return r_data