import peewee

import tornado.web
import tornado.gen
import tornado.escape

from settings import t_settings as env
from windseed.base import handler


class Handler(handler.Handler):
    def write_error(self, status_code, **kwargs):
        self.render('web/error.html', status_code=status_code)


class ErrorHandler(tornado.web.ErrorHandler, Handler):
    pass


class SitemapHandler(Handler):
    """
    Sitemap: /sitemap/
    """
    def get_page_context(self):
        """
        Return current page context
        """

        return dict()

    @tornado.gen.coroutine
    def get(self):
        """
        Render sitemap
        """
        self.set_header('Content-Type', 'text/xml')
        self.render(
            'web/sitemap.xml',
            **self.get_page_context())
