# -*- coding: utf-8 -*-

import tornado.web
from tornado.web import asynchronous
from tornado import gen
from tornado import httpclient
from easysettings import EasySettings

from settings import t_settings
from windseed.settings import db


from pymemcache.client.base import Client
client = Client(('localhost', 11211))


class BaseHandler(tornado.web.RequestHandler):
    """
    base class for others handlers

    базовый класс для остальных хендлеров
    """

    context = dict()
    settings_all = False
    settings_obj = False
    mclient = client

    # def __init__(self, *args, **kwargs):
    #     self.settings_obj = EasySettings(t_settings.ROOT_PATH+"tennis.conf")
    #     self.settings_all = self.get_setttings()
    #     self.context['set'] = self.settings_all
    #     super(GetSettings, self).__init__(*args, **kwargs)

    def get_setttings(self):
        all_s = self.settings_obj.list_settings()
        e_settings = dict()
        for i in all_s:
            e_settings[i[0]] = i[1]

        return e_settings


    def prepare(self):
        """
        Prepare db connection pool
        """
        db.pool.connect()

        return super().prepare()

    def on_finish(self):
        """
        Close db connection pool
        """
        if not db.pool.is_closed():
            db.pool.close()

        return super().on_finish()


    def initialize(self):
        self.context = dict()
        self.settings_obj = EasySettings(t_settings.ROOT_PATH+"tennis.conf")
        self.settings_all = self.get_setttings()
        self.context['settings'] = self.settings_all


    def get(self):
        """
        if in inherit class not define GET show 404
        если в дочернем классе не определён метод get покажем 404 страницу
        """
        self.set_status(404)
        self.write('{"status":"error","msg":"Page not found"}')


    def _get_safe_argument(self, name):
        """
        т.к. работа с POST данными в торнадо не удобная приходится немого повозиться
        родной get_argument сразу возвращает 404, а это не удобно,
        приходится брать get_arguments который возвращяет словарь.
        если нам вернули несоклько переменных, то мы отдаём последнюю
        """
        arg = self.get_arguments(name)
        if len(arg) >= 1:
            return arg[-1]
        else:
            return False


    # def get_error_html(self, status_code, **kwargs):
        # self.write(self.render_string("404.html"))


    def set_json_type(self):
        self.set_header("Content-Type", "application/json")

    def message_get(self):
        """
        возвращает выводимое сообщение, т.к. сообщение должно показываться один раз то и удаляем его
        """
        msg = self.mclient.get('message')
        self.mclient.delete('message')
        if msg:
            return msg.decode(encoding='UTF-8')
        else:
            return msg

    def message_set(self, message):
        return self.mclient.set('message', message.encode('utf-8'))
