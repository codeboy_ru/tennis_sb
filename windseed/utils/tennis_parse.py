import peewee
import json
from bs4 import BeautifulSoup
import requests
import argparse

import tornado.gen
import tornado.ioloop

# from settings import t_settings as env
# from windseed.settings import db
# from windseed.apps.web.models import Record, RecordPage

from windseed.apps.tennis.models import Drawsheet, Player, Tournament


@tornado.gen.coroutine
def main():
    # test_selenium()

    tw = TourWork()
    tw.start()

    # cookies = get_cookie()
    # ds_urls = find_ds_links(cookies)
    # parse_ds(ds_urls)

    # db.pool.connect()

class TourWork(object):
    tour_key = False
    cookies = False
    ds_base_url = False
    tour = False

    def __init__(self):
        self.tour_key = 'M-FU-ESP-10A-2017'
        self.cookies = {'visid_incap_178373': 'OMd6q2k0RB223BulB3XpCx5T4lgAAAAAQUIPAAAAAAATfDXGGvUg6LOvBJ3ZfEGw',
                        'incap_ses_583_178373':'XNiSFj/yeSxO4OAzjDsXCB5T4lgAAAAAJkdJsvJPvQALhRVbWzOd7A=='}
        self.ds_base_url = 'http://www.itftennis.com/itf/web/usercontrols/tournaments/tournamentdrawsheets.aspx?eventid={0}&webletName=procircuit'



    def start(self):
        tour = self.get_tour()
        self.tour = tour
        cookies = self.get_cookie()
        ds_urls = self.find_ds_links(cookies)


    def get_tour(self):
        """
            создаём или просто берём турнир
        """
        try:
            tour = Tournament.get(Tournament.itf_code == self.tour_key)
        except Tournament.DoesNotExist:
            tour = Tournament.create(name = 'autoname - {0}'.format(self.tour_key), itf_code = self.tour_key)
            tour.save()
        # print(tour)
        return tour


    def get_cookie(self):
        """
        пробуем получить куки со страницы матча
        пока не работает
        """
        s = requests.Session()
        s.headers.update({'referer': 'http://itfprocircuit.tennis-live-scores.com/?tid=W-WITF-FRA-09A-2017'})
        # r = s.get('http://itfprocircuit.tennis-live-scoress.com/?tid=W-WITF-FRA-09A-2017')
        r = s.get('http://www.itftennis.com/procircuit/home.aspx')
        # r = s.get("http://www.itftennis.com/procircuit/tournaments/women's-tournament/info.aspx?tourkey=W-WITF-FRA-09A-2017")
        cookies = s.cookies.get_dict()
        print(cookies)

        cookies = self.cookies
        return cookies


    def find_ds_links(self, cookies):
        """
            парсим страницу чемпионата и находим ссылки на турнитную таблицу (drawsheet)
            для каждой найденой ссылки вызываем перебор по игрокам и запись в базу
            :param cookies:
            :return:
        """

        tour_key = self.tour_key
        url_tour_info = "http://www.itftennis.com/procircuit/tournaments/women's-tournament/info.aspx?tourkey={0}"\
            .format(tour_key)
        # url_tour_info = "http://www.itftennis.com/procircuit/tournaments/women's-tournament/info.aspx?tourkey=W-WITF-FRA-09A-2017"
        # page = requests.get(url_tour_info)

        s = requests.Session()
        # r = s.get(url_tour_info)
        # cookies = s.cookies.get_dict()

        # s.headers.update({'referer': 'http://itfprocircuit.tennis-live-scores.com/?tid=W-WITF-FRA-09A-2017'})
        # s.headers.update({'referer': url_tour_info})


        page = s.get(url_tour_info, cookies=cookies)
        # print(page.url)
        # print(page.text)

        soup = BeautifulSoup(page.content, 'html.parser')
        titleTag = soup.html.head.title
        if titleTag:
            # print(titleTag)
            # print(soup.prettify())
            ds_urls = soup.find_all('a', {'class':'lnkLinkToDrawsheet'})
            # print(ds_urls)
            for i in ds_urls:
                s = i['onclick']
                s = s[s.find("(")+1:s.find(")")]
                s = s.split(',')[0]
                # print(s)
                self.parse_ds(s, cookies)

            return ds_urls

        else:
            print('NOOOOOOOO')


    def parse_ds(self, ds_urls, cookies):
        """
            обрабатываем турнирную таблицу с игроками
            добавляем их в базу если нет и в турнир
            :param ds_urls:
            :param cookies:
        """
        # print('--------------------------------------------------')
        url= self.ds_base_url.format(ds_urls)
        s = requests.Session()
        page = s.get(url, cookies=cookies)
        # print(page.url)

        soup = BeautifulSoup(page.content, 'html.parser')

        playesr_top = soup.find_all('a', {'id':'lnkPlayerTop1'})
        playesr_bot = soup.find_all('a', {'id':'lnkPlayerBottom1'})
        # print(playesr_top)
        # print(len(playesr_top))
        # print(len(playesr_bot))

        for i in playesr_top:
            name = i.string
            itf_id = i.get('href').split('=')[1]

            # добавляем игрока в базу
            try:
                player = Player.get(Player.itf_id == itf_id)
            except Player.DoesNotExist:
                player = Player.create(name=name, itf_name=name, itf_id=itf_id)
                player.save()

            # добавляем игрока в турнир
            try:
                ds = Drawsheet.get(Drawsheet.players == player.uid, Drawsheet.tournament == self.tour.uid)
            except Drawsheet.DoesNotExist:
                ds = Drawsheet.create(players=player.uid, tournament=self.tour.uid)
                ds.save()

        ### shit code mode on
        for i in playesr_bot:
            name = i.string
            itf_id = i.get('href').split('=')[1]

            # добавляем игрока в базу
            try:
                player = Player.get(Player.itf_id == itf_id)
            except Player.DoesNotExist:
                player = Player.create(name=name, itf_name=name, itf_id=itf_id)
                player.save()

            # добавляем игрока в турнир
            try:
                ds = Drawsheet.get(Drawsheet.players == player.uid, Drawsheet.tournament == self.tour.uid)
            except Drawsheet.DoesNotExist:
                ds = Drawsheet.create(players=player.uid, tournament=self.tour.uid)
                ds.save()




from selenium import webdriver
from pyvirtualdisplay import Display
def test_selenium():
    display = Display(visible=0, size=(800, 600))
    display.start()

    tour_key = 'W-WITF-FRA-10A-2017'
    url_tour_info = "http://www.itftennis.com/procircuit/tournaments/women's-tournament/info.aspx?tourkey={0}"\
        .format(tour_key)

    driver = webdriver.Firefox()
    # browser.get("http://docs.python-requests.org")
    driver.get(url_tour_info)
    print(driver.get_cookies())
    driver.refresh()
    html_source = driver.page_source
    # if "body" in html_source: print('11111111')
    # else: print('00000000000')
    print(html_source)
    # a = driver.find_elements_by_class_name("lnkLinkToDrawsheet")
    # print(a)

    soup = BeautifulSoup(html_source, 'html.parser')
    titleTag = soup.html.head.title
    if titleTag:
        # print(titleTag)
        # print(soup.prettify())
        ds_urls = soup.find_all('a', {'class':'lnkLinkToDrawsheet'})
        for i in ds_urls:
            s = i['onclick']
            s = s[s.find("(")+1:s.find(")")]
            s = s.split(',')[0]
    else:
        print('NOOOOOOOO')

    driver.close()
    display.stop()


@tornado.gen.coroutine
def test_request():
    tour_key = 'W-WITF-FRA-10A-2017'
    url_tour_info = "http://www.itftennis.com/procircuit/tournaments/women's-tournament/info.aspx?tourkey={0}"\
        .format(tour_key)

    s = requests.Session()
    # s.headers.update({'referer': 'http://itfprocircuit.tennis-live-scores.com/?tid=W-WITF-FRA-09A-2017'})
    s.headers.update({'referer': 'http://itfprocircuit.tennis-live-scores.com'})
    # s.headers.update({'referer': url_tour_info})
    r = s.get(url_tour_info)
    # cookies = s.cookies.get_dict()

    # print(r.request)
    # print(r.headers['Set-Cookie'])
    # print(r.headers.values())
    # for c in r.cookies:
    #     print(c.name, c.value)

    # zzz = r.headers['Set-Cookie']
    # print(zzz)


    s2 = requests.Session()
    s2.headers.update({'referer': 'http://itfprocircuit.tennis-live-scores.com'})
    cookies = s.cookies.get_dict()
    print(cookies)

    page = s.get(url_tour_info, cookies=s.cookies)
    # print(dir(page))
    # print(page.headers)
    print(page.url)
    # print(page.request.headers)
    print(page.cookies.get_dict())
    # print(page.text)


if __name__ == '__main__':
    # ioloop = tornado.ioloop.IOLoop.instance().run_sync(main)
    # ioloop = tornado.ioloop.IOLoop.instance().run_sync(test_request)
    ioloop = tornado.ioloop.IOLoop.instance().run_sync(test_selenium)
