from windseed.apps.admin.urls import routes as admin_routes
from windseed.apps.tennis.urls import routes as tennis_routes
from windseed.apps.tennis_ws.urls import routes as tennis_ws_routes

import settings.t_settings as TS
import tornado.web

routes = admin_routes + tennis_routes + tennis_ws_routes
routes += [
            (r"/static/(.*)", tornado.web.StaticFileHandler, {"path": TS.STATIC_PATH}),
        ]
