import os
from settings import t_settings

def check_img(handler, itf_id, *args, **kwargs):
    result = os.path.isfile('{0}/{1}.jpg'.format(t_settings.UPLOAD_PATH, itf_id))
    return result