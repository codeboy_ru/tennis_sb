EventsApp = function () {
    this.params = {
//        'base': 'on'
    };

    this.init = function (params) {
        $.extend({}, this.params, params);
        var a = this;

        a.$profile = $('.events-profile');
        a.$link = $('.events-link');

        a.playerLiveId = false;
        a.playerName = false;

        a.starterLoadContent();
        a.bindEvents();

    };

    /**
     *   EVENTS & ACTIVATORS
     *******************************************************/
    this.bindEvents = function () {
        this.$profile.on('click', eventsApp.profileClick);
        this.$link.on('click', eventsApp.linkClick);
        //$('.p-list-elem').on('click', eventsApp.onClickAdd);
        $(document).on('click', '.p-list-elem', eventsApp.onClickAdd);
    };


    /********************************************
     *  FUNCTIONS
     *******************************************/

    this.starterLoadContent = function () {
    };

    this.profileClick = function(e){
        var playerId = $(this).data('player-id');
        window.location.href = "/settings/profilebylive/"+playerId;
    };

    this.linkClick = function(e){
        //console.log($(this).data('player-id'));
        var playerId = $(this).data('player-id');
        var tourId = $(this).data('tour-id');
        var playerName = $(this).data('player-name');
        eventsApp.playerLiveId = playerId;
        eventsApp.playerName = playerId;

        $.ajax({
            url: '/settings/events/playerslist/'+tourId,
            data: '',
            method: 'get',
            dataType: 'json',
            success: function(data){
                //console.log(e);
                //console.log(data);
                //console.log(data.tour_id);
                eventsApp.createModal(data, playerId, tourId, playerName);
            }
        });
    };

    this.createModal = function(data, playerId, tourId, playerName){
        var modalList = $('.modal-list');
        modalList.empty();

        $.each(data.list, function(e, data){
            var txt = '';
            if (data.circ_id){
                txt += '<li class="list-group-item bg-success">'
            } else {
                txt+='<li class="list-group-item">'
            }
            txt += '<input class="btn btn-primary btn-xs p-list-elem" type="button" value="+" data-uid="'
                +data.uid+'" data-live-id="'+playerId+'" data-tour-id="'+tourId+'"><b class="name">'+data.name+'</b></li>';
            modalList.append(txt)
        });

        $('.modal-title').text('Связать - '+playerName);

        var options = {
            valueNames: ['name']
            //page: 5,
            //pagination: true
        };
        var userList = new List('users', options);
        $('.modal-body #users').find('input.search').focus();

        $('#myModal').modal();
    };

    this.onClickAdd = function(){
        //console.log($(this).data('uid'));
        //console.log($(this).data('live-id'));
        var uid = $(this).data('uid');
        var liveid = $(this).data('live-id');
        var tourid = $(this).data('tour-id');

        $.ajax({
            url: '/settings/events/playerslist/'+tourid,
            data: {
                uid : uid,
                liveid : liveid
            },
            method: 'post',
            dataType: 'json',
            success: function(data){
                if (data.result == 'good'){
                    location.reload();
                }
            }
        });
    }


};


$(document).ready(function () {

    eventsApp = new EventsApp();
    eventsApp.init();


    $("[name='screenSwitch']").bootstrapSwitch({
        on: 'ON',
        off: 'OFF ',
        onLabel: 'on',
        offLabel: 'off',
        same: false,//same labels for on/off states
        size: 'md',
        onClass: 'success',
        offClass: 'default'
    });
    $("[name='board_reload']").bootstrapSwitch({
        on: 'ON',
        off: 'OFF ',
        onLabel: 'on',
        offLabel: 'off',
        same: false,//same labels for on/off states
        size: 'md',
        onClass: 'success',
        offClass: 'default'
    });

    $("[name='color_switch']").bootstrapSwitch({
        on: 'Светлый',
        off: 'Тёмный ',
        onLabel: 'Светлый',
        offLabel: 'Тёмный',
        same: false,//same labels for on/off states
        size: 'md',
        onClass: 'secondary',
        offClass: 'default'
    });

});