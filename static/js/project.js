BaseApp = function () {
    this.params = {
//        'base': 'on'
    };

    this.init = function (params) {
        $.extend({}, this.params, params);
        var a = this;

//        a.$content = $('#content');

        a.$courtItems = $('.court');

        a.$matchId = $('#matchId').text();
        a.$courtId = $('#courtId').text();
        a.$ws_url = $('#ws_url').text();
        a.$ws_port = $('#ws_port').text();
        a.$timePlayed = $('#time_played').text();

        a.$timer = $('#timer-block');

        //a.$resHome = $('tr.home-row td.game-result');
        a.$resHome = $('.game-result.home');
        //a.$resAway = $('tr.away-row td.game-result');
        a.$resAway = $('.game-result.away');

        //a.$resHomeP1 = $('tr.home-row td.set1-result');
        a.$resHomeP1 = $('.set1-result.home');
        //a.$resHomeP2 = $('tr.home-row td.set2-result');
        a.$resHomeP2 = $('.set2-result.home');
        //a.$resHomeP3 = $('tr.home-row td.set3-result');
        a.$resHomeP3 = $('.set3-result.home');
        a.$resHomeP4 = $('tr.home-row td.set4-result');
        a.$resHomeP5 = $('tr.home-row td.set5-result');

        //a.$resAwayP1 = $('tr.away-row td.set1-result');
        a.$resAwayP1 = $('.set1-result.away');
        //a.$resAwayP2 = $('tr.away-row td.set2-result');
        a.$resAwayP2 = $('.set2-result.away');
        //a.$resAwayP3 = $('tr.away-row td.set3-result');
        a.$resAwayP3 = $('.set3-result.away');
        a.$resAwayP4 = $('tr.away-row td.set4-result');
        a.$resAwayP5 = $('tr.away-row td.set5-result');

        //a.$resHomeServe = $('tr.home-row td.serve-indicator span');
        a.$resHomeServe = $('td.serve-indicator-home span');
        //a.$resAwayServe = $('tr.away-row td.serve-indicator span');
        a.$resAwayServe = $('td.serve-indicator-away span');

        a.service = 0;
        a.service_on = 1;
        a.wsc = false;

        a.starterLoadContent();
        a.bindEvents();

    };

    /**
     *   EVENTS & ACTIVATORS
     *******************************************************/
    this.bindEvents = function () {
        //this.$courtItems.on('click', baseApp.courtClick);

        //this.$resAwayP2.hide()
    };


    /********************************************
     *  FUNCTIONS
     *******************************************/

    this.starterLoadContent = function () {
//        this.request_event();

        baseApp.make_timer(timer);

        if($('#courtId').length) {
            this.ws_connect(this.$ws_url, this.$ws_port);
            this.request_changes2();
        }
    };

    this.make_timer = function(r_time){
        var m_toint = parseInt(r_time['m']);
        var h_toint = parseInt(r_time['h']);
        if (m_toint >= 10) {
            baseApp.$timer.text(r_time['h'] + ' : ' + r_time['m'])
        } else {
            baseApp.$timer.text(r_time['h'] + ' : 0' + r_time['m'])
        }
    };


    this.check_changes = function(element, value, blink){
        //console.log(element.html(), value);
        //console.log(typeof element.html(), typeof value);
        if(element.html() != value){
            blink = typeof blink !== 'undefined' ? blink : 1;

            //console.log('NO');
            element.html(value);
            var start_color = element.css('background-color');
            if(blink==1) {
                element.animate({backgroundColor: "#c0b371"}, 500)
                    .animate({backgroundColor: start_color}, 900);
            }
        } else {
            //console.log('YES');
        }
    };

    this.serve_change = function(value){
        //console.log('f-serve', value);
        if(value >= 1){
            //console.log('serve 1111');
            if (value == 1) {
                baseApp.$resHomeServe.show();
                baseApp.$resAwayServe.hide();
                baseApp.service = 1
            } else {
                baseApp.$resHomeServe.hide();
                baseApp.$resAwayServe.show();
                baseApp.service = 2
            }
        } else {
            //console.log('serve 0000');
            if(baseApp.service_on==0) {
                //console.log('serve 33333');
                if (baseApp.service == 2) {
                    baseApp.$resHomeServe.show();
                    baseApp.$resAwayServe.hide();
                    baseApp.service = 1
                } else {
                    baseApp.$resHomeServe.hide();
                    baseApp.$resAwayServe.show();
                    baseApp.service = 2
                }
                baseApp.service_on = 1
            }
        }
    };

    
    this.show_scoreboard = function(data) {
        //console.log(data);
        //console.log('gp =', data['game_points']);
        //console.log('service =', data['service']);
        //console.log('periods =', data['periods']);

        // result
        if (data['game_points'] != 0) {
            //console.log(data['game_points']['home']);
            if (data['game_points']['home'] == 50) {
                baseApp.check_changes(baseApp.$resHome, 'AD', blink = 1);
            } else {
                console.log('');
                baseApp.check_changes(baseApp.$resHome, data['game_points']['home'], blink = 1);
            }
            if (data['game_points']['away'] == 50) {
                baseApp.check_changes(baseApp.$resAway, 'AD', blink = 1);
            } else {
                baseApp.check_changes(baseApp.$resAway, data['game_points']['away'], blink = 1);
            }
        }

        //serve
        //if (data['service'] != 0){
        //this.serve_change(data['service']);
        this.serve_change(data['game_service']['service']);
        //}

        // periods
        if (data['periods'] != 0) {
            baseApp.$resHomeP1.html(data['periods']['p1']['home']);

            if (data['periods']['p2'] != null) {
                baseApp.check_changes(baseApp.$resHomeP2, data['periods']['p2']['home']);
            }


            if (data['periods']['p3'] != null) {
                baseApp.$resHomeP3.show();
                baseApp.check_changes(baseApp.$resHomeP3, data['periods']['p3']['home']);
            }
            if (data['periods']['p4'] != null) {
                baseApp.$resHomeP4.show();
                baseApp.check_changes(baseApp.$resHomeP4, data['periods']['p4']['home']);
            }
            if (data['periods']['p5'] != null) {
                baseApp.$resHomeP5.show();
                baseApp.check_changes(baseApp.$resHomeP5, data['periods']['p5']['home']);
            }

            baseApp.$resAwayP1.html(data['periods']['p1']['away']);
            if (data['periods']['p2'] != null) {
                baseApp.$resAwayP2.html(data['periods']['p2']['away']);
            }

            if (data['periods']['p3'] != null) {
                baseApp.$resAwayP3.show();
                baseApp.$resAwayP3.html(data['periods']['p3']['away']);
            }
            if (data['periods_p4'] != null) {
                baseApp.$resAwayP4.show();
                baseApp.$resAwayP4.html(data['periods']['p4']['away']);
            }
            if (data['periods']['p5'] != null) {
                baseApp.$resAwayP5.show();
                baseApp.$resAwayP5.html(data['periods']['p5']['away']);
            }
        }
    };
    
    this.ws_connect = function(host, port){
        var uri = '/ws/';
        var ws_url = "ws://" + host + ":" + port + uri;

        var ws = new WebSocket(ws_url);
        baseApp.wsc = ws;
        console.log('websocket connect');


        ws.onmessage = function(evt) {
            var msg = JSON.parse(evt.data);
            console.log(msg);
            console.log(msg['data']['ev_type_id'], msg['data']['ev_type_name'], msg['data']['ev_type_type']);
            var update_table = true;

            if(msg['board_reload'] == 'on'){
                location.reload();
            } else {

                if (msg['screen_status'] == 'on') {
                    $('.table-wrapper').hide();
                    $('#message h2').html(msg['message']);
                    $('#message').show();
                } else {
                    if (msg['screen_status'] != 'wait') {
                        if ($('#message').is(":visible")) {
                            //console.log('visible');
                            $('.table-wrapper').show();
                            $('#message').hide();
                        }
                    }

                    // MAIN SECTION !!!!!!!!!!!!!
                    if (msg['status'] == 'live') {
                        if (msg['data'] != 0) {
                            baseApp.make_timer(msg['data']['time_div']);
                            var event_type = msg['data']['ev_type_type'];
                            var event_name = msg['data']['ev_type_name'];
                            var event_id = msg['data']['ev_type_id'];

                            if (event_type == 'periodscore') {
                                baseApp.check_changes(baseApp.$resHome, 0, blink = 1);
                                baseApp.check_changes(baseApp.$resAway, 0, blink = 1);
                                baseApp.serve_change(0);
                            }

                            if (event_type == 'periodscore' && event_name == '1st set') {
                            //if (event_type == 'score_change_tennis') {
                            //    $('#match_status').show();
                                $('#match_status').html('Match started');
                                $('#match_status').animate({backgroundColor: "#c0b371"}, 1500)
                                    .animate({backgroundColor: '#504846'}, 1500).fadeOut( "slow" );
                            }

                            if(event_id == '20'){
                                $('#match_status').html('Match played');
                                $('#match_status').show();
                                baseApp.check_changes(baseApp.$resHome, 0, blink = 1);
                                baseApp.check_changes(baseApp.$resAway, 0, blink = 1);

                            } else if (event_id == '1024'){
                                baseApp.new_match(msg['data']);

                                update_table = false;

                            } else if (event_id == '10'){
                                $('#match_status').html('Match started');
                                //$('#match_status').hide();
                                $('#match_status').animate({backgroundColor: "#c0b371"}, 1000)
                                    .animate({backgroundColor: '#504846'}, 1000).fadeOut( "slow" );
                                //location.reload();
                            }

                            if(update_table == true) {
                                baseApp.show_scoreboard(msg['data'])
                            }
                        }
                    } else if (msg['status'] == 'end') {
                        $('#match_status').show();
                    }
                }
            }

        };

        ws.onclose = function(){
            //try to reconnect in 5 seconds
            console.log('WS connection closed, try to restart.');
            setTimeout(function(){baseApp.ws_connect(baseApp.$ws_url, baseApp.$ws_port)}, 2000);
        };

    };

    this.new_match = function(msg){
        $('#match_status').html('Match about to start');
        $('#match_status').show();
        baseApp.check_changes(baseApp.$resHome, 0, blink = 1);
        baseApp.check_changes(baseApp.$resAway, 0, blink = 1);


        baseApp.$resHomeP1.html(0);
        baseApp.$resHomeP2.html(0);
        baseApp.$resHomeP3.hide().html(0);
        baseApp.$resHomeP4.hide().html(0);
        baseApp.$resHomeP5.hide().html(0);

        baseApp.$resAwayP1.html(0);
        baseApp.$resAwayP2.html(0);
        baseApp.$resAwayP3.hide().html(0);
        baseApp.$resAwayP4.hide().html(0);
        baseApp.$resAwayP5.hide().html(0);


        if(msg['team_home']['children']){
            var flags_html_home = '';
            var flags_html_away = '';
            var names_html_home = '';
            var names_html_away = '';
            var img_1 = '<img src="https://ls.sportradar.com/ls/crest/big/';
            var img_2 = '.png">';

            msg['team_home']['children'].forEach(function(entry) {
                flags_html_home += img_1+entry['cc']['a2']+img_2+'<br>';
                names_html_home += entry['name']+' <span>'+entry['cc']['ioc']+'</span><br>'
            });
            $('tr.home-row > td.player-flag').html(flags_html_home);
            $('tr.home-row > td.player-name').html(names_html_home);

            msg['team_away']['children'].forEach(function(entry) {
                flags_html_away += img_1+entry['cc']['a2']+img_2+'<br>';
                names_html_away += entry['name']+' <span>'+entry['cc']['ioc']+'</span><br>'
            });
            $('tr.away-row> td.player-flag').html(flags_html_away);
            $('tr.away-row > td.player-name').html(names_html_away);

        } else {

            var flag_url = 'https://ls.sportradar.com/ls/crest/big/';
            var p_flag_home = msg['team_home']['cc']['a2'];
            var p_flag_away = msg['team_away']['cc']['a2'];
            $('tr.home-row > td.player-flag > img').attr('src', flag_url + p_flag_home + '.png');
            $('tr.away-row > td.player-flag > img').attr('src', flag_url + p_flag_away + '.png');

            $('tr.home-row > td.player-name').html(msg['team_home']['name']+' <span>'+msg['team_home']['cc']['ioc']+'</span>');
            $('tr.away-row > td.player-name').html(msg['team_away']['name']+' <span>'+msg['team_away']['cc']['ioc']+'</span>');
        }
    };


    this.request_changes2 = function() {
        window.setInterval(function () {
            var ws_data = {
                'type':'get_events',
                'court_id':baseApp.$courtId
            };
            baseApp.wsc.send(JSON.stringify(ws_data));
        //}, 3000);
        }, 4500);
    };

};


$(document).ready(function () {

    baseApp = new BaseApp();
    baseApp.init();


/*

    function analyze(options) {
        console.log(options);
        smartcrop.crop(img, options, draw);
    }

    function faceDetectionJquery(options, img, callback) {
        $(img).faceDetection({
            complete: function (faces) {
                if (faces === false) {
                    return console.log('jquery.facedetection returned false');
                }
                console.log('jquery.facedetection detected ' + faces.length + ' faces', faces);
                options.boost = Array.prototype.slice.call(faces, 0).map(function (face) {
                    return {
                        x: face.x,
                        y: face.y,
                        width: face.width,
                        height: face.height,
                        weight: 1.0
                    };
                });

                callback();
            }
        });
    }

    function run(img) {
        console.log('zzzzzzzzz');
        if (!img) return;
        var options = {
            width: 100,
            height: 100,
            //minScale: form.minScale.value * 1,
            ruleOfThirds: true,
            //debug: true,
        };

        faceDetectionJquery(options, img, function () {analyze(options);});
        analyze(options);

    }

    $.each($('.img-row img'), function(a, b){
        console.log(b.src);

        //img = new Image();
        //img.src = b.src;
        //run(img);

        smartcrop.crop(b, {width: 100, height: 100}).then(function(result){
            console.log(result);
        });

    })
*/

});