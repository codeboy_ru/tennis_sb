import os
from os import path


# DEBUG = False
DEBUG = True

PORT = 8000
ROOT_PATH = os.path.join(os.path.dirname(__file__), '', '..').replace('\\','/')

WS_URL = ''
WS_PORT = PORT

AUTORELOAD = True

BASE_PATH = path.join(path.dirname(__file__), '../../')
# STATIC_PATH = os.path.join(os.path.dirname(__file__), '', '../static').replace('\\','/')
STATIC_PATH = path.join(BASE_PATH, 'static')
# TEMPLATE_PATH = os.path.join(os.path.dirname(__file__), '', '../templates').replace('\\','/')
TEMPLATE_PATH = path.join(BASE_PATH, 'templates')
UPLOAD_PATH = os.path.abspath(os.path.join(os.path.dirname( __file__ ), '..', 'static/uploads'))

DOMAIN = 'some-site.com'
SUPERUSER_EMAIL = 'superuser@windseed.com'
SUPERUSER_PASSWORD = 'superuser'

COOKIE_SECRET = '3378fc6097e1ce0f97f43d4c49ba31afc9c253abe9fb639829ffbc1866a56d2a'
# COOKIE_SECRET = getenv('WINDSEED_COOKIE_SECRET')  # secret key for secret cookie
XSRF_COOKIES = True



MAX_CONNECTIONS = 8 #32
STALE_TIMEOUT = 30 #120

RECORD_COUNT = 10000 # 1000000
RELATED_ITEMS_PER_PAGE = 10 #100
ADMIN_ITEMS_PER_PAGE = 20
RECORDS_PER_PAGE = 48
SITEMAP_PER_PAGE = 100

try:
    from settings.t_settings_local import *
except ImportError:
    pass
