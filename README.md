#Electronic tennis scoreboard.

Python3 + tornado + peewee

=============================


- убрать баг в таблице счёта если сетов больше 2х
- окончание матча


## Project based on Windseed

Extendable skeleton for developing web applications using [Tornado]
(https://github.com/tornadoweb/tornado) and [peewee]
(https://github.com/coleifer/peewee)

For its implementation using [aiohttp](https://github.com/KeepSafe/aiohttp) refer to [aiowing](https://github.com/embali/aiowing)


## Prerequisites

- Ubuntu 14.10
- PostgreSQL 9.3
- Python 3.5+
- nginx
- Supervisor


## Structure

- **backups** - folder for pg_dump to save dump using bash/backup.sh

- **bash** - various bash scripts including project settings within environment
             variables:
  - **backup.sh** - create pg_dump in backups folder
  - **env.sh** - contains all environment variables for project
  - **superuser.sh** - create superuser
  - **supervisor.sh** - script for supervisor
  - **tables.sh** - drop database, crate database and create db tables
  - **tests.sh** - run tests
  - **windseed.sh** - run application

- **conf** - config files for nginx and supervisor for development and production

- **static** - project static files, mainly Bootstrap 3, robots.txt, admin styles

- **templates** - project templates

- **windseed** - windseed package:
  - **apps** - project's applications folder, each app within its folder has:
    - **handlers.py** - app handlers
    - **models.py** - app models
    - **tests.py** - app tests
    - **urls.py** - app URLs and routes
  - **base** - base classes:
    - **handler.py** - base handler class
    - **model.py** - base model class
    - **test.py** - base test class
  - **settings** - project settings
    - **db.py** - postgresql database pool
    - **env.py** - environment variables from env.sh and paths
    - **urls.py** - project routes
  - **utils** - project utilities:
    - **superuser.py** - create superuser
    - **tables.py** - create tables

- **windseed.py** - Windseed Tornado application


## Install Python 3.5+
```
sudo add-apt-repository ppa:fkrull/deadsnakes

sudo apt-get update

sudo apt-get install python3.5 python3.5-venv python3.5-dev
```

## Setup environment and packages

```
pyvenv-3.5 .env

source .env/bin/activate

pip install -r requirements.txt
```

## Generate cookie secret

```
openssl rand -base64 40
```
and put it to **env.sh**


## Run

```
bash scripts/tables.sh - create db and tables

bash scripts/superuser.sh - create superuser

bash scripts/tests.sh - run tests

bash scripts/windseed.sh - run project
```

## Nginx

```
sudo apt-get install nginx

sudo cp conf/prod-nginx.conf /etc/nginx/nginx.conf

sudo service nginx restart
```

## Ubuntu/nginx file limits

**/etc/security/limits.conf**:
```
soft nofile 16384

hard nofile 16384
```

**/etc/sysctl.conf**:
```
fs.file-max = 16384
```

**/etc/pam.d/common-session**:
```
session required pam_limits.so
```

## SSL certificate

```
sudo openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout windseed.key -out windseed.crt
```

## Supervisor

```
sudo apt-get install supervisor

pgrep -fl supervisor

sudo service supervisor start

sudo cp conf/supervisord.conf /etc/supervisor/supervisord.conf

sudo supervisord -c conf/supervisord.conf

sudo service supervisor restart

sudo unlink /tmp/supervisor.sock

kill related processes
```

## Performance

Hardware:

- Intel® Core™ i7-5500U CPU @ 2.40GHz ? 4
- 16 Gb RAM

Testing with [wrk] (https://github.com/wg/wrk):

- 10 000, 100 000, 1 000 000 records in table
- 1 minute test with 1 minute timeout
- 1 and 10 threads
- From 10 to 100 connections with 10 connections step
- Pagination using SQL OFFSET and pagination using pagination table
- First and last page
- Sorted by name
- Compare Windseed and [Django] (https://github.com/embali/windseed-django)
apps with similar functionality

Start wrk
```
wrk -t1 -c10 -d1m --timeout 1m "http://localhost:8000/?page=1"
```

Start Windseed
```
bash bash/windseed.sh
```

Start Django
```
uwsgi --module=djangotest.wsgi:application
      --env DJANGO_SETTINGS_MODULE=djangotest.settings
      --http=127.0.0.1:8000
      --processes 1
```


= время таймер
- заголовок капитал
= бэкграунд
= шрифт
= вернуть флаг
= seed - посев выводить
- проверить новый матч
- название турнира в админке
- фотки 300*300
= функционал xml - алиас для кортов
- лого турнира справа
- перезагрузка страницы - рассылать всем подключившимся
- если нет матча на корте 500
- png в картинки