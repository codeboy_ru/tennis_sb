# coding: utf-8


from time import gmtime, strftime
from apps.BaseHandler import BaseHandler
from tornado import gen
import asyncio
import requests

import asyncio
import json

from apps.BaseAsyncHandler import AsyncRequestHandler
# from database.models import Flight
# from database.db_queries import get_flights

class GameInfoAH(BaseHandler):


    def get(self, param1):
        context = dict()
        context = self.context

        matchs = self._load_data(param1)
        context['matchs'] = matchs
        context['match'] = matchs[0]

        #-------------------
        self.render('scoreboard.html', **context)



    def _load_data(self, court_id):
        s = requests.Session()
        # s.headers.update({
        #         'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:45.0) Gecko/20100101 Firefox/45.0'
        #     })

        date_str = strftime("%Y%m%d", gmtime())
        url = 'https://ls.sportradar.com/ls/feeds/?/itf/en/Europe:Berlin/gismo/client_dayinfo/{0}'.format(date_str)
        # url = 'https://ls.sportradar.com/ls/feeds/?/itf/en/Europe:Berlin/gismo/client_dayinfo/{0}'.format('20160422')
        r2 = s.get(url)
        day_info = r2.json()
        day_info = day_info['doc'][0]['data']['matches']

        r_data = list()

        for k,v in day_info.items():
            if v['match'].get('court') and v['match']['court']['_id'] == int(court_id):
                r_game = dict()
                s_id = v['match']['status']['_id']

                if 1 < s_id < 80:
                    s_name = v['match']['status']['name']

                    p_home_name = v['match']['teams']['home']['name']
                    p_home_c_code = v['match']['teams']['home']['cc']['_id']
                    p_home_c_name = v['match']['teams']['home']['cc']['name']
                    p_home_c_sname = v['match']['teams']['home']['cc']['ioc']
                    p_home_c_fname = v['match']['teams']['home']['cc']['a2']

                    p_away_name = v['match']['teams']['away']['name']
                    p_away_c_code = v['match']['teams']['away']['cc']['_id']
                    p_away_c_name = v['match']['teams']['away']['cc']['name']
                    p_away_c_sname = v['match']['teams']['away']['cc']['ioc']
                    p_away_c_fname = v['match']['teams']['away']['cc']['a2']

                    court_name = v['match']['court']['name']
                    court_id = v['match']['court']['_id']

                    result = v['match']['result']
                    gs_who = v['match']['gamescore']['service']
                    gs_home = v['match']['gamescore']['home']
                    gs_away = v['match']['gamescore']['away']

                    default_result = {
                        'home': 0,
                        'away': 0
                    }

                    periods_p1 = v['match']['periods'].get('p1', default_result)
                    periods_p2 = v['match']['periods'].get('p2', default_result)
                    periods_p3 = v['match']['periods'].get('p3')
                    periods_p4 = v['match']['periods'].get('p4')
                    periods_p5 = v['match']['periods'].get('p5')

                    match_id = v['_id']

                    # print(k, court_name)
                    # print(s_name, s_id)
                    # print(p_home_name, p_away_name)
                    # print(result)
                    # print(gs_who)
                    # print(type(periods_p1))
                    # print('---------------')

                    r_game = {
                        's_id':s_id,
                        's_name':s_name,
                        'court_name':court_name,
                        'court_id':court_id,
                        'p_home_name': p_home_name,
                        'p_home_c_sname':p_home_c_sname,
                        'p_home_c_fname':p_home_c_fname,
                        'p_away_name':p_away_name,
                        'p_away_c_sname':p_away_c_sname,
                        'p_away_c_fname':p_away_c_fname,
                        'gs_who':gs_who,
                        'gs_home':gs_home,
                        'gs_away':gs_away,
                        'periods_p1':periods_p1,
                        'periods_p2':periods_p2,
                        'periods_p3':periods_p3,
                        'periods_p4':periods_p4,
                        'periods_p5':periods_p5,
                        'match_id':match_id
                    }
                    r_data.append(r_game)

        return r_data


class GameChangesAH(BaseHandler):

    def get(self, court_id):
        context = dict()
        context = self.context

        matchs = self._load_data(court_id)
        context['match'] = matchs[0]

        #-------------------
        # self.render('scoreboard.html', **context)
        response = matchs[0]

        self.write(response)



    def _load_data(self, court_id):
        s = requests.Session()
        # s.headers.update({
        #         'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:45.0) Gecko/20100101 Firefox/45.0'
        #     })

        date_str = strftime("%Y%m%d", gmtime())
        url = 'https://ls.sportradar.com/ls/feeds/?/itf/en/Europe:Berlin/gismo/client_dayinfo/{0}'.format(date_str)
        # url = 'https://ls.sportradar.com/ls/feeds/?/itf/en/Europe:Berlin/gismo/client_dayinfo/{0}'.format('20160422')
        r2 = s.get(url)
        day_info = r2.json()
        day_info = day_info['doc'][0]['data']['matches']

        r_data = list()

        for k,v in day_info.items():
            if v.get('match') and v['match'].get('_tid') and \
                    v['match'].get('court') and v['match']['court']['_id'] == int(court_id):
                r_game = dict()
                s_id = v['match']['status']['_id']

                if 1 < s_id < 100:
                    s_name = v['match']['status']['name']

                    p_home_name = v['match']['teams']['home']['name']
                    p_home_c_code = v['match']['teams']['home']['cc']['_id']
                    p_home_c_name = v['match']['teams']['home']['cc']['name']
                    p_home_c_sname = v['match']['teams']['home']['cc']['ioc']
                    p_home_c_fname = v['match']['teams']['home']['cc']['a2']

                    p_away_name = v['match']['teams']['away']['name']
                    p_away_c_code = v['match']['teams']['away']['cc']['_id']
                    p_away_c_name = v['match']['teams']['away']['cc']['name']
                    p_away_c_sname = v['match']['teams']['away']['cc']['ioc']
                    p_away_c_fname = v['match']['teams']['away']['cc']['a2']

                    court_name = v['match']['court']['name']
                    court_id = v['match']['court']['_id']

                    result = v['match']['result']
                    gs_who = v['match']['gamescore']['service']
                    gs_home = v['match']['gamescore']['home']
                    gs_away = v['match']['gamescore']['away']

                    default_result = {
                        'home': 0,
                        'away': 0
                    }

                    periods_p1 = v['match']['periods'].get('p1', default_result)
                    periods_p2 = v['match']['periods'].get('p2', default_result)
                    periods_p3 = v['match']['periods'].get('p3')
                    periods_p4 = v['match']['periods'].get('p4')
                    periods_p5 = v['match']['periods'].get('p5')

                    match_id = v['_id']

                    # print(s_name, s_id)
                    # print(p_home_name, p_away_name)
                    # print(result)
                    # print(gs_home, gs_away)
                    # print(type(periods_p1))
                    # print('---------------')

                    r_game = {
                        's_id':s_id,
                        's_name':s_name,
                        'court_name':court_name,
                        'p_home_name': p_home_name,
                        'p_home_c_sname':p_home_c_sname,
                        'p_home_c_fname':p_home_c_fname,
                        'p_away_name':p_away_name,
                        'p_away_c_sname':p_away_c_sname,
                        'p_away_c_fname':p_away_c_fname,
                        'gs_who':gs_who,
                        'gs_home':gs_home,
                        'gs_away':gs_away,
                        'periods_p1':periods_p1,
                        'periods_p2':periods_p2,
                        'periods_p3':periods_p3,
                        'periods_p4':periods_p4,
                        'periods_p5':periods_p5,
                        'match_id':match_id
                    }
                    r_data.append(r_game)

        return r_data