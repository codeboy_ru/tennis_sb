
import json
from time import gmtime, strftime
from apps.BaseHandler import BaseHandler
from tornado import gen
# import asyncio
import requests
from easysettings import EasySettings

from settings import t_settings

# from apps.BaseAsyncHandler import AsyncRequestHandler
# from database.db_queries import get_users



from pymemcache.client.base import Client
client = Client(('localhost', 11211))


class AdminHandler(BaseHandler):

    def get(self):
        context = dict()
        context = self.context
        # context['status'] = "work in progress"

        # context['courts'] = self._load_data()

        # settings = EasySettings("tennis.conf")
        settings = EasySettings(t_settings.ROOT_PATH+"tennis.conf")
        # settings.set("screen_status", False)

        context['court_id'] = settings.get('court_id')
        context['match_id'] = settings.get('match_id')
        context['tour_id'] = settings.get('tour_id')
        context['screen_text'] = settings.get('screen_text')
        context['screen_status'] = settings.get('screen_status')
        context['color_status'] = settings.get('color_status')
        # print(context)

        ids_list = client.get('ids_list')
        # client.flush_all()

        # event_ids_list = client.get('event_ids_list')
        # print('---:', event_ids_list)
        # bytesThing = event_ids_list.decode(encoding='UTF-8')
        # event_mem_keys = (bytesThing.split(','))
        # event_mem_all = client.get_many(event_mem_keys)
        # print(len(event_mem_all))
        # for k,v in event_mem_all.items():
        #     print(k, v)

        #-------------------
        self.render('admin.html', **context)

    def post(self, *args, **kwargs):
        # settings = EasySettings("tennis.conf")
        settings = EasySettings(t_settings.ROOT_PATH+"tennis.conf")

        tour_id = self.get_argument("tour_id", default=None, strip=False)
        if tour_id: settings.set('tour_id', tour_id)

        court_id = self.get_argument("court_id", default=None, strip=False)
        if court_id: settings.set('court_id', court_id)

        screen_text = self.get_argument("screen_text", default=None, strip=False)
        if screen_text:
            settings.set('screen_text', screen_text)

        if self.get_argument("screenSwitch", default=None, strip=False):
            settings.set('screen_status', True)
            client.set('screen_status', 'on')
        else:
            settings.set('screen_status', False)
            client.set('screen_status', 'off')

        if self.get_argument("board_reload", default=None, strip=False):
            client.set('board_reload', 'on')

        if self.get_argument("color_switch", default=None):
            settings.set('color_status', True)
        else:
            settings.set('color_status', False)

        settings.save()
        self.redirect('/settings')


