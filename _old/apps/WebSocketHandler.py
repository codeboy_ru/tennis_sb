# coding: utf-8

from tornado import websocket

import json
import datetime
import requests
from easysettings import EasySettings

from settings import t_settings


from pymemcache.client.base import Client
client = Client(('localhost', 11211))


class WSHandler(websocket.WebSocketHandler):
    clients = []

    def open(self):
        # print('new connection')
        WSHandler.clients.append(self)

    def on_message(self, message):
        msg = json.loads(message)
        court_id = msg.get('court_id')
        screen_status = 'off'
        board_reload = 'off'
        r_data = 0
        r_message = ''

        # print(msg)
        # Reverse Message and send it back
        # self.write_message(message[::-1])

        if client.get('board_reload') and client.get('board_reload').decode(encoding='UTF-8') == 'on':
            # board_reload = 'on'
            for i in WSHandler.clients:
                i.write_message({'board_reload':'on'})

            client.set('board_reload', 'off')

        else:

            screen_status_raw = client.get('screen_status')
            if screen_status_raw:
                screen_status = screen_status_raw.decode(encoding='UTF-8')


            if screen_status == 'on':
                settings = EasySettings(t_settings.ROOT_PATH+"tennis.conf")
                r_message = settings.get('screen_text')

            else:
                screen_status = 'off'
                r_data = self._get_events(court_id)
                # print(r_data)
                if r_data == 0:
                    client.set('screen_status', 'wait')
                    screen_status = 'wait'


        response = {
            'status':'live',
            'message':r_message,
            'data':r_data,
            'screen_status':screen_status,
            'board_reload':board_reload
        }
        # print(response)
        # print('~~~~~~~~~~~~~~~~')

        self.write_message(response)


    def on_close(self):
        # print('connection closed')
        WSHandler.clients.remove(self)

    def check_origin(self, origin):
        return True


    def _get_events(self, court_id):
        # print(self.request)
        # print(self.request.host)
        if self.request.host == '192.168.56.101:8081':
            ws_url = '192.168.56.101'
            ws_port = '8081'

        s = requests.Session()
        url = 'https://ls.sportradar.com/ls/feeds/?/itf/en/Europe:Berlin/gismo/event_getitf'
        r = s.get(url)

        events = r.json()
        events = events['doc'][0]['data']

        events_list = list()
        events_dict = dict()

        for event in events:
            # print('matchstatus: ', event['match']['matchstatus'])
            if event['match'].get('court') and event['match']['court']['_id'] == int(court_id):
                # print(event)
                ev_data = dict()
                ev_data['ev_type_type'] = event['type']
                ev_data['ev_type_id'] = event['_typeid']
                ev_data['ev_type_name'] = event['name']

                ev_data['match_id'] = event['matchid']

                # live, recentlyended, upcoming
                ev_data['match_status'] = event['match']['matchstatus']
                ev_data['court_name'] = event['match']['court']['name']
                ev_data['court_id'] = event['match']['court']['_id']

                ev_data['team_home'] = event['match']['teams']['home']
                ev_data['team_away'] = event['match']['teams']['away']

                ev_data['service'] = event.get('service', 0)

                ev_data['set_score'] = event.get('set_score', 0)
                ev_data['game_score'] = event.get('game_score', 0)
                ev_data['game_points'] = event.get('game_points', 0)

                ev_data['period'] = event.get('p', 0)

                ev_data['periods'] = event['match'].get('periods', 0)
                # print('per - ', event['match'].get('periods', 0))

                # ev_data['full_event'] = event


                events_list.append(event['_id'])
                events_dict[event['_id']] = ev_data
                # print('###########')
                # print(events_list)
                # print(events_dict)
                # print('###########')

        if len(events_list) >= 1:
            max_event = max(events_list)
            response_data = events_dict[max_event]
            return response_data

        else:
            return 0



