# coding: utf-8

import asyncio
from datetime import datetime
import peewee_async
import peewee as p


database = p.Proxy()


class BaseModel(p.Model):
    """  initialize database, need for poxy  """
    class Meta:
        database = database

class Flight(BaseModel):
    STATUS_OK = 'OK'
    STATUS_INAIR = 'In Air'
    STATUS_ARRIVED = 'Arrived'
    STATUS_DEPARTED = 'Departed'
    STATUS_LANDED = 'Landed'
    STATUS_SCHEDULED = 'Scheduled'
    STATUS_DELAYED = 'Delayed'
    STATUS_CANCELED = 'Canceled'

    STATUS_CHOICES = ((1, STATUS_OK), (2, STATUS_INAIR), (3, STATUS_ARRIVED), (4, STATUS_DEPARTED),
                      (5, STATUS_LANDED), (6, STATUS_SCHEDULED), (7, STATUS_DELAYED), (8, STATUS_CANCELED),)

    flight = p.CharField()
    airline = p.CharField()
    destination = p.CharField()
    fly_type = p.BooleanField(help_text='If true flight departs')
    fly_datetime = p.DateTimeField(formats='%d-%m-%Y %H:%M:%S', default=datetime.now())
    status = p.IntegerField(choices=STATUS_CHOICES, help_text='')
    terminal = p.CharField()

    class Meta:
        order_by = ('fly_datetime',)

    def __unicode__(self):
        return '{0}-{1}'.format(self.flight, self.airline)