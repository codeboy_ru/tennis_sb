# coding: utf-8

import tornado.web
from apps import (
    HomeHandler,
    GamesHandler,
    AdminHandler,
    CourtHandler,

    # ErrorsHandler,
    # UserHandler,
    # CardParsing,

    WebSocketHandler,
    WS_TestDataHandler
)


# there is routes for all parts of project
ROUTES = [
    (r'/', GamesHandler.GameInfoHandler),
    (r'/settings/?', AdminHandler.AdminHandler),
    (r'/courts/?', CourtHandler.CourtHandler),
    (r'/courts/(?P<court>[^\/]+)/?', CourtHandler.CourtHandler),
    (r'/scoreboard/(?P<param1>[^\/]+)/?', GamesHandler.GameInfoHandler),
    (r'/scoreboard-change/(?P<court_id>[^\/]+)/?', GamesHandler.GameChangesAH),
    (r'/ws/', WebSocketHandler.WSHandler),
    (r'/wst/', WS_TestDataHandler.WSHandler),

]